# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact provides means to visualize (state of) board games or (state of) [`cellular automatons`](https://en.wikipedia.org/wiki/Cellular_automaton). The idea behind this artifact is to get a `breadboard` visualizing anything which can by placed on a `checkerboard`. ***

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-checkerboard</artifactId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-checkerboard). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-checkerboard).

## Introduction ##

> "In the mid 80's I was playing around with [`Atari BASIC`](https://en.wikipedia.org/wiki/Atari_BASIC), [`GFA BASIC`](https://en.wikipedia.org/wiki/GFA_BASIC) and [`Turbo Pascal`](https://en.wikipedia.org/wiki/Turbo_Pascal). At that time I was also eagerly reading articles published in the magazine [Spektrum DER WISSENSCHAFT](http://www.spektrum.de) from my father's bookshelf. I was especially interested in articles belonging to the columns Mathematische Unterhaltung (Mathematical Games) and Computer Kurzweil (Computer Recreations). Here the authors worked out and explained on how to get mathematical and logical puzzles solved or simulated with an ordinary Home Computer. Quite a few of those puzzles were "played" by the computer on some kind of two dimensional grid or matrix. Lately a special edition with Computer Kurzweil experiments tumbled out of my bookshelf, memories came to mind and as a result I set up the [`refcodes-checkerboard`](https://bitbucket.org/refcodes/refcodes-checkerboard) artifact ..." [Siegfried Steiner, Munich, 2015-06-05]

*[Spektrum DER WISSENSCHAFT](http://www.spektrum.de) - see also [Spektrum der Wissenschaft](https://de.wikipedia.org/wiki/Spektrum_der_Wissenschaft) ([Wikipedia](https://de.wikipedia.or)) and [International editions](https://en.wikipedia.org/wiki/Scientific_American#International_editions) ([Wikipedia](https://en.wikipedia.org)). [Spektrum DER WISSENSCHAFT](http://www.spektrum.de) is the German edition of the American magazine [SCIENTIFC AMERICAN](http://www.scientificamerican.com/) - see also [Scientific American](https://en.wikipedia.org/wiki/Scientific_American) ([Wikipedia](https://en.wikipedia.org)).* 

## How do I get started? ##

See the sources of the [`funcodes-watchdog`](https://bitbucket.org/funcodez/funcodes-watchdog) command line tool for a demo if the `API`.

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-checkerboard/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.