module org.refcodes.checkerboard {
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.controlflow;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.factory;
	requires transitive org.refcodes.graphical;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.observer;
	requires transitive org.refcodes.struct;
	requires org.refcodes.textual;
	requires org.refcodes.runtime;

	exports org.refcodes.checkerboard;
}
