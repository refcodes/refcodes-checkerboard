// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

/**
 * Provides an accessor for a {@link Direction} property.
 */
public interface DirectionAccessor {

	/**
	 * Retrieves the {@link Direction} from the {@link Direction} property.
	 * 
	 * @return The {@link Direction} stored by the {@link Direction} property.
	 */
	Direction getDirection();

	/**
	 * Provides a mutator for a {@link Direction} property.
	 */
	public interface DirectionMutator {

		/**
		 * Sets the {@link Direction} for the {@link Direction} property.
		 * 
		 * @param aDirection The {@link Direction} to be stored by the sprite
		 *        factory property.
		 */
		void setDirection( Direction aDirection );
	}

	/**
	 * Provides a builder method for a {@link Direction} property returning the
	 * builder for applying multiple build operations.
	 */
	public interface DirectionBuilder<B extends DirectionBuilder<B>> {

		/**
		 * Sets the {@link Direction} for the {@link Direction} property.
		 * 
		 * @param aDirection The {@link Direction} to be stored by the sprite
		 *        factory property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withDirection( Direction aDirection );
	}

	/**
	 * Provides a {@link Direction} property.
	 */
	public interface DirectionProperty extends DirectionAccessor, DirectionMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Direction}
		 * (setter) as of {@link #setDirection(Direction)} and returns the very
		 * same value (getter).
		 * 
		 * @param aDirection The {@link Direction} to set (via
		 *        {@link #setDirection(Direction)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Direction letDirection( Direction aDirection ) {
			setDirection( aDirection );
			return aDirection;
		}
	}
}
