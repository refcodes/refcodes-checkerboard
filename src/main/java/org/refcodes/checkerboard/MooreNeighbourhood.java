// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

/**
 * Definitions of the Moore neighbourhood.
 */
public enum MooreNeighbourhood implements Neighbourhood<MooreNeighbourhood> {

	LEFT(-1, 0), TOP_LEFT(-1, -1), TOP(0, -1), TOP_RIGHT(1, -1), RIGHT(1, 0), BOTTOM_RIGHT(1, 1), BOTTOM(0, 1), BOTTOM_LEFT(-1, 1);

	private int _posX;
	private int _posY;

	private MooreNeighbourhood( int aPosX, int aPosY ) {
		_posX = aPosX;
		_posY = aPosY;
	}

	/**
	 * Returns the relative X position to position (0, 0).
	 * 
	 * @return The relative position X to the (0,0) coordinate.
	 */
	@Override
	public int getPositionX() {
		return _posX;
	}

	/**
	 * Returns the relative Y position to position (0, 0).
	 * 
	 * @return The relative position Y to the (0,0) coordinate.
	 */
	@Override
	public int getPositionY() {
		return _posY;
	}

	/**
	 * Returns the next clockwise state relative to the current state. Imagine
	 * the states as the positions on a clock, next to {@link #TOP} would be
	 * {@link #TOP_RIGHT}, next to {@link #TOP_RIGHT} would be {@link #RIGHT},
	 * and so on (clockwise).
	 * 
	 * @return The next state relative to the current state.
	 */
	@Override
	public MooreNeighbourhood clockwiseNext() {
		return switch ( this ) {
		case TOP -> TOP_RIGHT;
		case TOP_RIGHT -> RIGHT;
		case RIGHT -> BOTTOM_RIGHT;
		case BOTTOM_RIGHT -> BOTTOM;
		case BOTTOM -> BOTTOM_LEFT;
		case BOTTOM_LEFT -> LEFT;
		case LEFT -> TOP_LEFT;
		case TOP_LEFT -> TOP;
		};
	}

	/**
	 * Returns the next anti-clockwise state relative to the current state.
	 * Imagine the states as the positions on a clock, next to {@link #TOP}
	 * would be {@link #TOP_LEFT}, next to {@link #TOP_LEFT} would be
	 * {@link #LEFT}, and so on (anti-clockwise).
	 * 
	 * @return The next state relative to the current state.
	 */
	@Override
	public MooreNeighbourhood clockwisePrevious() {
		return switch ( this ) {
		case TOP -> TOP_LEFT;
		case TOP_LEFT -> LEFT;
		case LEFT -> BOTTOM_LEFT;
		case BOTTOM_LEFT -> BOTTOM;
		case BOTTOM -> BOTTOM_RIGHT;
		case BOTTOM_RIGHT -> RIGHT;
		case RIGHT -> TOP_RIGHT;
		case TOP_RIGHT -> TOP;
		};
	}
}
