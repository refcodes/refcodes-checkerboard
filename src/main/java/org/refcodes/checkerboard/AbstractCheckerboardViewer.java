package org.refcodes.checkerboard;

import org.refcodes.component.InitializeException;
import org.refcodes.graphical.Dimension;
import org.refcodes.graphical.GridMode;
import org.refcodes.graphical.Offset;
import org.refcodes.graphical.Position;
import org.refcodes.graphical.ViewportDimension;
import org.refcodes.graphical.ViewportDimensionPropertyBuilder;
import org.refcodes.graphical.ViewportOffset;
import org.refcodes.observer.SubscribeEvent;
import org.refcodes.observer.UnsubscribeEvent;

/**
 * In order to provide a {@link Checkerboard}, register an observer by invoking
 * {@link Checkerboard#subscribeObserver(Object)}. The default
 * {@link Checkerboard#subscribeObserver(Object)} method will invoke this
 * {@link #onSubscribe(SubscribeEvent)} method which in turn sets the
 * {@link Checkerboard} instance. Retrieve it by calling {@link #_checkerboard}
 *
 * @param <P> the generic type
 * @param <S> The type which's instances represent a {@link Player} state.
 * @param <IMG> The type for the state's representation ("image").
 * @param <SF> the generic type
 * @param <CBV> The {@link CheckerboardViewer}'s type implementing this
 *        interface.
 */
public abstract class AbstractCheckerboardViewer<P extends Player<P, S>, S, IMG, SF extends SpriteFactory<IMG, S, ?>, CBV extends CheckerboardViewer<P, S, CBV>> implements CheckerboardViewer<P, S, CBV> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _viewportWidth = -1;
	private int _viewportHeight = -1;
	private int _viewportOffsetX = 0;
	private int _viewportOffsetY = 0;
	private final ViewportDimensionPropertyBuilder _minViewportDimension = new ViewportDimensionPropertyBuilder( -1, -1 );
	protected Checkerboard<P, S> _checkerboard;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link CheckerboardViewer} for the given
	 * {@link Checkerboard}.
	 * 
	 * @param aCheckerboard The {@link Checkerboard} for which to construct the
	 *        viewer.
	 */
	public AbstractCheckerboardViewer( Checkerboard<P, S> aCheckerboard ) {
		_checkerboard = aCheckerboard;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * With initialize.
	 *
	 * @return the cbv
	 * 
	 * @throws InitializeException the initialize exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withInitialize() throws InitializeException {
		initialize();
		return (CBV) this;
	}

	/**
	 * With viewport offset Y.
	 *
	 * @param aPosY the pos Y
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withViewportOffsetY( int aPosY ) {
		setViewportOffsetY( aPosY );
		return (CBV) this;
	}

	/**
	 * With viewport height.
	 *
	 * @param aGridHeight the grid height
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withViewportHeight( int aGridHeight ) {
		setViewportHeight( aGridHeight );
		return (CBV) this;
	}

	/**
	 * With viewport width.
	 *
	 * @param aGridWidth the grid width
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withViewportWidth( int aGridWidth ) {
		setViewportWidth( aGridWidth );
		return (CBV) this;
	}

	/**
	 * With viewport dimension.
	 *
	 * @param aWidth the width
	 * @param aHeight the height
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withViewportDimension( int aWidth, int aHeight ) {
		setViewportDimension( aWidth, aHeight );
		return (CBV) this;
	}

	/**
	 * With viewport dimension.
	 *
	 * @param aGridDimension the grid dimension
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withViewportDimension( ViewportDimension aGridDimension ) {
		setViewportDimension( aGridDimension );
		return (CBV) this;
	}

	/**
	 * With viewport dimension.
	 *
	 * @param aDimension the dimension
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withViewportDimension( Dimension aDimension ) {
		setViewportDimension( aDimension );
		return (CBV) this;
	}

	/**
	 * With viewport offset.
	 *
	 * @param aPosX the pos X
	 * @param aPosY the pos Y
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withViewportOffset( int aPosX, int aPosY ) {
		setViewportOffset( aPosX, aPosY );
		return (CBV) this;
	}

	/**
	 * With viewport offset.
	 *
	 * @param aOffset the offset
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withViewportOffset( Position aOffset ) {
		setViewportOffset( aOffset );
		return (CBV) this;
	}

	/**
	 * With viewport offset.
	 *
	 * @param aOffset the offset
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withViewportOffset( ViewportOffset aOffset ) {
		setViewportOffset( aOffset );
		return (CBV) this;
	}

	/**
	 * With viewport offset.
	 *
	 * @param aOffset the offset
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withViewportOffset( Offset aOffset ) {
		setViewportOffset( aOffset );
		return (CBV) this;
	}

	/**
	 * With viewport offset X.
	 *
	 * @param aPosX the pos X
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withViewportOffsetX( int aPosX ) {
		setViewportOffsetX( aPosX );
		return (CBV) this;
	}

	/**
	 * With min viewport dimension.
	 *
	 * @param aDimension the dimension
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withMinViewportDimension( ViewportDimension aDimension ) {
		setMinViewportDimension( aDimension );
		return (CBV) this;
	}

	/**
	 * With min viewport dimension.
	 *
	 * @param aWidth the width
	 * @param aHeight the height
	 * 
	 * @return the cbv
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withMinViewportDimension( int aWidth, int aHeight ) {
		setMinViewportDimension( aWidth, aHeight );
		return (CBV) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportDimension( int aWidth, int aHeight ) {
		if ( _minViewportDimension.getViewportWidth() != -1 && aWidth < _minViewportDimension.getViewportWidth() ) {
			throw new IllegalArgumentException( "The provided grid width <" + aWidth + "> is less than the min grid width (<" + _minViewportDimension.getViewportWidth() + ">)." );
		}
		if ( _minViewportDimension.getViewportHeight() != -1 && aHeight < _minViewportDimension.getViewportHeight() ) {
			throw new IllegalArgumentException( "The provided grid height <" + aHeight + "> is less than the min grid height (<" + _minViewportDimension.getViewportHeight() + ">)." );
		}
		_viewportWidth = aWidth;
		_viewportHeight = aHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportDimension( ViewportDimension aGridDimension ) {
		setViewportDimension( aGridDimension.getViewportWidth(), aGridDimension.getViewportHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportDimension( Dimension aDimension ) {
		setViewportDimension( aDimension.getWidth(), aDimension.getHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportWidth( int aGridWidth ) {
		setViewportDimension( aGridWidth, _viewportHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportHeight( int aGridHeight ) {
		setViewportDimension( _viewportWidth, aGridHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportWidth() {
		return _viewportWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportHeight() {
		return _viewportHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffset( int aPosX, int aPosY ) {
		if ( aPosX != _viewportOffsetX || aPosY != _viewportOffsetY ) {
			final ViewportOffsetChangedEvent<P, S> theEvent = new ViewportOffsetChangedEvent<>( aPosX, aPosY, _viewportOffsetX, _viewportOffsetY, this );
			_viewportOffsetX = aPosX;
			_viewportOffsetY = aPosY;
			onViewportOffsetChangedEvent( theEvent );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffset( Position aOffset ) {
		setViewportOffset( aOffset.getPositionX(), aOffset.getPositionY() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffset( ViewportOffset aOffset ) {
		setViewportOffset( aOffset.getViewportOffsetX(), aOffset.getViewportOffsetY() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffset( Offset aOffset ) {
		setViewportOffset( aOffset.getOffsetX(), aOffset.getOffsetY() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffsetX( int aPosX ) {
		setViewportOffset( aPosX, _viewportOffsetY );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportOffsetX() {
		return _viewportOffsetX;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportOffsetY() {
		return _viewportOffsetY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffsetY( int aPosY ) {
		setViewportOffset( _viewportOffsetX, aPosY );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMinViewportDimension( ViewportDimension aDimension ) {
		_minViewportDimension.setViewportDimension( aDimension );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ViewportDimension getMinViewportDimension() {
		return _minViewportDimension;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMinViewportDimension( int aWidth, int aHeight ) {
		_minViewportDimension.setViewportDimension( aWidth, aHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMinViewportDimension( Dimension aDimension ) {
		_minViewportDimension.setViewportDimension( aDimension );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CBV withMinViewportDimension( Dimension aDimension ) {
		setMinViewportDimension( aDimension );
		return (CBV) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void centerViewportOffset( int aPositionX, int aPositionY ) {
		centerViewportOffset( aPositionX, aPositionY, getViewportWidth(), getViewportHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void centerViewportOffset( int aPositionX, int aPositionY, int aViewportWidth, int aViewportHeight ) {
		final int theGridWidth = getGridWidth();
		final int theGridHeight = _checkerboard.getGridHeight();
		int theOffsetX = aPositionX - ( aViewportWidth / 2 );
		int theOffsetY = aPositionY - ( aViewportHeight / 2 );
		if ( theOffsetX > theGridWidth - aViewportWidth ) {
			theOffsetX = theGridWidth - aViewportWidth;
		}
		if ( theOffsetY > theGridHeight - aViewportHeight ) {
			theOffsetY = theGridHeight - aViewportHeight;
		}
		if ( theOffsetX < 0 ) {
			theOffsetX = 0;
		}
		if ( theOffsetY < 0 ) {
			theOffsetY = 0;
		}
		setViewportOffset( -theOffsetX, -theOffsetY );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GridMode getGridMode() {
		return _checkerboard.getGridMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGridWidth() {
		return _checkerboard.getGridWidth();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGridHeight() {
		return _checkerboard.getGridHeight();
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		_checkerboard.destroy();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSubscribe( SubscribeEvent<Checkerboard<P, S>> aSubscribeEvent ) {
		_checkerboard = aSubscribeEvent.getSource();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onUnsubscribe( UnsubscribeEvent<Checkerboard<P, S>> aUnsubscribeEvent ) {}
}
