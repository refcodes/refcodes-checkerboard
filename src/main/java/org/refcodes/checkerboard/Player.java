package org.refcodes.checkerboard;

import org.refcodes.exception.VetoException;
import org.refcodes.graphical.DraggableAccessor.DraggableBuilder;
import org.refcodes.graphical.DraggableAccessor.DraggableProperty;
import org.refcodes.graphical.Position.PositionBuilder;
import org.refcodes.graphical.Position.PositionProperty;
import org.refcodes.graphical.VisibleAccessor.VisibleBuilder;
import org.refcodes.graphical.VisibleAccessor.VisibleProperty;
import org.refcodes.mixin.StatusAccessor.StatusBuilder;
import org.refcodes.mixin.StatusAccessor.StatusMutator;
import org.refcodes.mixin.StatusAccessor.StatusProperty;
import org.refcodes.observer.Observable;

/**
 * A {@link Player} defines an actor (player) on a playground (checkerboard). It
 * is observable in order to signal observers (checkerboard) on state updates.
 *
 * @param <P> the {@link Player} generic type.
 * @param <S> The state of the player (dead, alive, strong, weak, king, queen,
 *        Pac-Man, ghost, fire, wood, water, strong queen, weak king, etc).
 */
public interface Player<P extends Player<P, S>, S> extends PositionProperty, PositionBuilder<P>, Observable<PlayerObserver<P, S>>, StatusMutator<S>, StatusProperty<S>, StatusBuilder<S, P>, VisibleProperty, VisibleBuilder<P>, DraggableProperty, DraggableBuilder<P> {

	/**
	 * Causes the {@link Player} to blink.
	 */
	default void blink() {
		for ( int i = 0; i < 6; i++ ) {
			setVisible( !isVisible() );
			try {
				Thread.sleep( 150 );
			}
			catch ( InterruptedException ignore ) {}
		}
	}

	/**
	 * Moves the player one to the right.
	 */
	default void moveRight() {
		setPosition( getPositionX() + 1, getPositionX() );
	}

	/**
	 * Moves the player one to the left.
	 */
	default void moveLeft() {
		setPosition( getPositionX() - 1, getPositionX() );
	}

	/**
	 * Moves the player one up.
	 */
	default void moveUp() {
		setPosition( getPositionX(), getPositionX() - 1 );
	}

	/**
	 * Moves the player one down.
	 */
	default void moveDown() {
		setPosition( getPositionX(), getPositionX() - 1 );
	}

	/**
	 * Selects ("clicks") the player.
	 */
	void click();

	/**
	 * Registers an according observer. For unregistering, use the returned
	 * handle.
	 * 
	 * @param aObserver The observer to be registered.
	 * 
	 * @return The accordingly registered {@link PlayerObserver}.
	 */
	default PlayerObserver<P, S> onChangePosition( ChangePlayerPositionObserver<P> aObserver ) {
		final PlayerObserver<P, S> theObserver = new PlayerObserver<P, S>() {
			@Override
			public void onPlayerEvent( PlayerEvent<P> aPlayerEvent ) {}

			@Override
			public void onChangePlayerPositionEvent( ChangePlayerPositionEvent<P> aPlayerEvent ) throws VetoException {
				aObserver.onChangePlayerPositionEvent( aPlayerEvent );
			}
		};
		subscribeObserver( theObserver );
		return theObserver;
	}

	/**
	 * Registers an according observer. For unregistering, use the returned
	 * handle.
	 * 
	 * @param aObserver The observer to be registered.
	 * 
	 * @return The accordingly registered {@link PlayerObserver}.
	 */
	default PlayerObserver<P, S> onPositionChanged( PlayerPositionChangedObserver<P> aObserver ) {
		final PlayerObserver<P, S> theObserver = new PlayerObserver<P, S>() {
			@Override
			public void onPlayerEvent( PlayerEvent<P> aPlayerEvent ) {}

			@Override
			public void onPlayerPositionChangedEvent( PlayerPositionChangedEvent<P> aPlayerEvent ) {
				aObserver.onPlayerPositionChangedEvent( aPlayerEvent );
			}
		};
		subscribeObserver( theObserver );
		return theObserver;
	}

	/**
	 * Registers an according observer. For unregistering, use the returned
	 * handle.
	 * 
	 * @param aObserver The observer to be registered.
	 * 
	 * @return The accordingly registered {@link PlayerObserver}.
	 */
	default PlayerObserver<P, S> onStateChanged( PlayerStateChangedObserver<P, S> aObserver ) {
		final PlayerObserver<P, S> theObserver = new PlayerObserver<P, S>() {
			@Override
			public void onPlayerEvent( PlayerEvent<P> aPlayerEvent ) {}

			@Override
			public void onPlayerStateChangedEvent( PlayerStateChangedEvent<P, S> aPlayerEvent ) {
				aObserver.onPlayerStateChangedEvent( aPlayerEvent );
			}
		};
		subscribeObserver( theObserver );
		return theObserver;
	}

	/**
	 * Registers an according observer. For unregistering, use the returned
	 * handle.
	 * 
	 * @param aObserver The observer to be registered.
	 * 
	 * @return The accordingly registered {@link PlayerObserver}.
	 */
	default PlayerObserver<P, S> onVisibilityChanged( PlayerVisibilityChangedObserver<P> aObserver ) {
		final PlayerObserver<P, S> theObserver = new PlayerObserver<P, S>() {
			@Override
			public void onPlayerEvent( PlayerEvent<P> aPlayerEvent ) {}

			@Override
			public void onPlayerVisibilityChangedEvent( PlayerVisibilityChangedEvent<P> aPlayerEvent ) {
				aObserver.onPlayerVisibilityChangedEvent( aPlayerEvent );
			}
		};
		subscribeObserver( theObserver );
		return theObserver;
	}

	/**
	 * Registers an according observer. For unregistering, use the returned
	 * handle.
	 * 
	 * @param aObserver The observer to be registered.
	 * 
	 * @return The accordingly registered {@link PlayerObserver}.
	 */
	default PlayerObserver<P, S> onDraggabilityChanged( PlayerDraggabilityChangedObserver<P> aObserver ) {
		final PlayerObserver<P, S> theObserver = new PlayerObserver<P, S>() {
			@Override
			public void onPlayerEvent( PlayerEvent<P> aPlayerEvent ) {}

			@Override
			public void onPlayerDraggabilityChangedEvent( PlayerDraggabilityChangedEvent<P> aPlayerEvent ) {
				aObserver.onPlayerDraggabilityChangedEvent( aPlayerEvent );
			}
		};
		subscribeObserver( theObserver );
		return theObserver;
	}

	/**
	 * Registers an according observer. For unregistering, use the returned
	 * handle.
	 * 
	 * @param aObserver The observer to be registered.
	 * 
	 * @return The accordingly registered {@link PlayerObserver}.
	 */
	default PlayerObserver<P, S> onClicked( PlayerClickedObserver<P> aObserver ) {
		final PlayerObserver<P, S> theObserver = new PlayerObserver<P, S>() {
			@Override
			public void onPlayerEvent( PlayerEvent<P> aPlayerEvent ) {}

			@Override
			public void onPlayerClickedEvent( PlayerClickedEvent<P> aPlayerEvent ) {
				aObserver.onPlayerClickedEvent( aPlayerEvent );
			}
		};
		subscribeObserver( theObserver );
		return theObserver;
	}
}
