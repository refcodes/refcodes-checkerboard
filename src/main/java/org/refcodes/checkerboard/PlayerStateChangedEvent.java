package org.refcodes.checkerboard;

import org.refcodes.mixin.StatusAccessor;

/**
 * The class PlayerStateChangedEvent.
 *
 * @param <P> the generic type
 * @param <S> The state of the player (dead, alive, strong, weak, king, queen,
 *        PacMan, ghost, fire, wood, water, strong queen, weak king, etc).
 */
public class PlayerStateChangedEvent<P extends Player<P, S>, S> extends AbstractPlayerEvent<P> implements StatusAccessor<S> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final PlayerAction ACTION = PlayerAction.STATE_CHANGED;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final S _state;
	private final S _precedingState;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new state changed event.
	 *
	 * @param aState the state
	 * @param aPrecedingState the preceding state
	 * @param aSource The according source (origin).
	 */
	public PlayerStateChangedEvent( S aState, S aPrecedingState, P aSource ) {
		super( ACTION, aSource );
		_state = aState;
		_precedingState = aPrecedingState;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public S getStatus() {
		return _state;
	}

	/**
	 * Gets the preceding state.
	 *
	 * @return the preceding state
	 */
	public S getPrecedingState() {
		return _precedingState;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return super.toString() + ", state := " + _state + " (state before: " + _precedingState + ")";
	}
}
