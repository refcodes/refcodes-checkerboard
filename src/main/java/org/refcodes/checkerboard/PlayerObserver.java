package org.refcodes.checkerboard;

import org.refcodes.exception.VetoException;

/**
 * An asynchronous update interface for receiving notifications about Player
 * information as the Player is constructed.
 *
 * @param <P> the generic type
 * @param <S> the generic type
 */
@FunctionalInterface
public interface PlayerObserver<P extends Player<P, S>, S> extends PlayerClickedObserver<P>, ChangePlayerPositionObserver<P>, PlayerPositionChangedObserver<P>, PlayerStateChangedObserver<P, S>, PlayerVisibilityChangedObserver<P>, PlayerDraggabilityChangedObserver<P> {

	/**
	 * This method is called when information about an Player which was
	 * previously requested using an asynchronous interface becomes available.
	 *
	 * @param aPlayerEvent the player event
	 */
	void onPlayerEvent( PlayerEvent<P> aPlayerEvent );

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void onChangePlayerPositionEvent( ChangePlayerPositionEvent<P> aPlayerEvent ) throws VetoException {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void onPlayerPositionChangedEvent( PlayerPositionChangedEvent<P> aPlayerEvent ) {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void onPlayerStateChangedEvent( PlayerStateChangedEvent<P, S> aPlayerEvent ) {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void onPlayerVisibilityChangedEvent( PlayerVisibilityChangedEvent<P> aPlayerEvent ) {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void onPlayerDraggabilityChangedEvent( PlayerDraggabilityChangedEvent<P> aPlayerEvent ) {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void onPlayerClickedEvent( PlayerClickedEvent<P> aPlayerEvent ) {}
}
