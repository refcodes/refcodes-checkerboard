// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

import java.util.Collection;
import java.util.Set;

import org.refcodes.struct.Relation;

/**
 * The Interface Sprites.
 *
 * @author steiner
 * 
 * @param <SPS> the generic type
 * @param <S> the generic type
 * @param <IMG> the generic type
 */
public interface Sprites<SPS extends Sprites<SPS, S, IMG>, S, IMG> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * With sprite.
	 *
	 * @param aRelation the relation
	 * 
	 * @return the sps
	 */
	SPS withSprite( Relation<S, IMG> aRelation );

	/**
	 * With sprite.
	 *
	 * @param aStatus the status
	 * @param aSprite the sprite
	 * 
	 * @return the sps
	 */
	SPS withSprite( S aStatus, IMG aSprite );

	/**
	 * Removes the sprite.
	 *
	 * @param aStatus the status
	 * 
	 * @return the img
	 */
	IMG removeSprite( S aStatus );

	/**
	 * Clear sprites.
	 */
	void clearSprites();

	/**
	 * Checks for sprites.
	 *
	 * @return true, if successful
	 */
	boolean hasSprites();

	/**
	 * Sprite count.
	 *
	 * @return the int
	 */
	int spriteCount();

	/**
	 * Values.
	 *
	 * @return the collection
	 */
	Collection<IMG> values();

	/**
	 * States.
	 *
	 * @return the sets the
	 */
	Set<S> states();

	/**
	 * Gets the sprite.
	 *
	 * @param aStatus the status
	 * 
	 * @return the sprite
	 */
	IMG getSprite( S aStatus );

	/**
	 * Contains sprite.
	 *
	 * @param aStatus the status
	 * 
	 * @return true, if successful
	 */
	boolean containsSprite( S aStatus );

	/**
	 * Put sprite.
	 *
	 * @param aRelation the relation
	 * 
	 * @return the img
	 */
	IMG putSprite( Relation<S, IMG> aRelation );

	/**
	 * Put sprite.
	 *
	 * @param aStatus the status
	 * @param aSprite the sprite
	 * 
	 * @return the img
	 */
	IMG putSprite( S aStatus, IMG aSprite );

}
