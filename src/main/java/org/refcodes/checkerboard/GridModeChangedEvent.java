package org.refcodes.checkerboard;

import org.refcodes.graphical.GridMode;
import org.refcodes.graphical.GridModeAccessor;

/**
 * The Class GridModeChangedEvent.
 *
 * @param <P> the generic type
 * @param <S> the generic type
 */
public class GridModeChangedEvent<P extends Player<P, S>, S> extends AbstractCheckerboardEvent<P, S> implements GridModeAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final CheckerboardAction ACTION = CheckerboardAction.GRID_MODE_CHANGED;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final GridMode _gridMode;
	private final GridMode _precedingGridMode;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new grid mode changed event.
	 *
	 * @param aGridMode the grid mode
	 * @param aPrecedingGridMode the preceding grid mode
	 * @param aSource The according source (origin).
	 */
	public GridModeChangedEvent( GridMode aGridMode, GridMode aPrecedingGridMode, Checkerboard<P, S> aSource ) {
		super( ACTION, aSource );
		_gridMode = aGridMode;
		_precedingGridMode = aPrecedingGridMode;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GridMode getGridMode() {
		return _gridMode;
	}

	/**
	 * Gets the preceding grid mode.
	 *
	 * @return the preceding grid mode
	 */
	public GridMode getPrecedingGridMode() {
		return _precedingGridMode;
	}
}
