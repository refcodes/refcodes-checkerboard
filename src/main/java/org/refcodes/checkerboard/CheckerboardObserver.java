package org.refcodes.checkerboard;

import org.refcodes.exception.VetoException;
import org.refcodes.observer.ObservableObserver;

/**
 * An asynchronous update interface for receiving notifications about
 * Checkerboard information as the Checkerboard is constructed.
 *
 * @param <P> the generic type
 * @param <S> the generic type
 */
public interface CheckerboardObserver<P extends Player<P, S>, S> extends ObservableObserver<Checkerboard<P, S>> {

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aCheckerboardEvent the checkerboard event
	 */
	void onCheckerboardEvent( CheckerboardEvent<P, S> aCheckerboardEvent );

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aPlayerEvent the player event
	 * @param aSource The according source (origin).
	 */
	void onPlayerEvent( PlayerEvent<P> aPlayerEvent, Checkerboard<P, S> aSource );

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aCheckerboardEvent the checkerboard event
	 */
	default void onPlayerAddedEvent( PlayerAddedEvent<P, S> aCheckerboardEvent ) {}

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aCheckerboardEvent the checkerboard event
	 */
	default void onPlayerRemovedEvent( PlayerRemovedEvent<P, S> aCheckerboardEvent ) {}

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aCheckerboardEvent the checkerboard event
	 */
	default void onGridModeChangedEvent( GridModeChangedEvent<P, S> aCheckerboardEvent ) {}

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aCheckerboardEvent the checkerboard event
	 */
	default void onGridDimensionChangedEvent( GridDimensionChangedEvent<P, S> aCheckerboardEvent ) {}

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aCheckerboardEvent the checkerboard event
	 */
	default void onViewportOffsetChangedEvent( ViewportOffsetChangedEvent<P, S> aCheckerboardEvent ) {}

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aCheckerboardEvent the checkerboard event
	 */
	default void onViewportDimensionChangedEvent( ViewportDimensionChangedEvent<P, S> aCheckerboardEvent ) {}

	/**
	 * This method is called when a grid element of the checkerboard bas been
	 * clicked.
	 *
	 * @param aCheckerboardEvent the checkerboard event
	 */
	default void onGridPositionClickedEvent( GridPositionClickedEvent<P, S> aCheckerboardEvent ) {}

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aPlayerEvent the player event
	 * @param aSource The according source (origin).
	 * 
	 * @throws VetoException the veto exception
	 */
	default void onChangePlayerPositionEvent( ChangePlayerPositionEvent<P> aPlayerEvent, Checkerboard<P, S> aSource ) throws VetoException {}

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aPlayerEvent the player event
	 * @param aSource The according source (origin).
	 */
	default void onPlayerPositionChangedEvent( PlayerPositionChangedEvent<P> aPlayerEvent, Checkerboard<P, S> aSource ) {}

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aPlayerEvent the player event
	 * @param aSource The according source (origin).
	 */
	default void onPlayerStateChangedEvent( PlayerStateChangedEvent<P, S> aPlayerEvent, Checkerboard<P, S> aSource ) {}

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aPlayerEvent the player event
	 * @param aSource The according source (origin).
	 */
	default void onPlayerVisibilityChangedEvent( PlayerVisibilityChangedEvent<P> aPlayerEvent, Checkerboard<P, S> aSource ) {}

	/**
	 * This method is called when information about a {@link Checkerboard} which
	 * was previously requested using an asynchronous interface becomes
	 * available.
	 *
	 * @param aPlayerEvent the player event
	 * @param aSource The according source (origin).
	 */
	default void onPlayerDraggabilityChangedEvent( PlayerDraggabilityChangedEvent<P> aPlayerEvent, Checkerboard<P, S> aSource ) {}
}
