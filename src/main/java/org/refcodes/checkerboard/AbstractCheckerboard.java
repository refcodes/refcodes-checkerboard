// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.exception.VetoException;
import org.refcodes.graphical.Dimension;
import org.refcodes.graphical.GridDimension;
import org.refcodes.graphical.GridMode;
import org.refcodes.graphical.Position;
import org.refcodes.graphical.PositionImpl;
import org.refcodes.observer.AbstractObservable;
import org.refcodes.observer.Event;
import org.refcodes.observer.SubscribeEvent;
import org.refcodes.observer.UnsubscribeEvent;

/**
 * The Class CheckerboardImpl.
 *
 * @param <P> the generic type
 * @param <S> the generic type
 */
public abstract class AbstractCheckerboard<T extends Checkerboard<P, S>, P extends Player<P, S>, S> extends AbstractObservable<CheckerboardObserver<P, S>, Event<?>> implements Checkerboard<P, S> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final List<P> _players = new ArrayList<>();
	private final Map<Integer, Map<Integer, P>> _rowToColumn = new HashMap<>();
	private final Map<Integer, Map<Integer, P>> _columnToRow = new HashMap<>();
	private int _gridWidth = -1;
	private int _gridHeight = -1;
	private GridMode _gridMode = GridMode.NONE;
	private final CheckerboardPlayerObserver _observer = new CheckerboardPlayerObserver();

	// /////////////////////////////////////////////////////////////////////////
	// BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T withGridMode( GridMode aGridMode ) {
		setGridMode( aGridMode );
		return (T) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T withGridDimension( int aGridWidth, int aGridHeight ) {
		setGridDimension( aGridWidth, aGridHeight );
		return (T) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T withGridDimension( GridDimension aDimension ) {
		setGridDimension( aDimension );
		return (T) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T withGridDimension( Dimension aDimension ) {
		setGridDimension( aDimension );
		return (T) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T withGridWidth( int aWidth ) {
		setGridWidth( aWidth );
		return (T) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T withGridHeight( int aHeight ) {
		setGridHeight( aHeight );
		return (T) this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void forEach( Consumer<P> aConsumer ) {
		new ArrayList<>( _players ).forEach( aConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasAtPosition( Position aPos ) {
		return hasAtPosition( aPos.getPositionX(), aPos.getPositionY() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasAtPosition( int aPosX, int aPosY ) {
		final Position thePos = toAbsPos( aPosX, aPosY );
		final Map<Integer, P> theColumn = _rowToColumn.get( thePos.getPositionX() );
		if ( theColumn != null ) {
			return theColumn.containsKey( thePos.getPositionY() );
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P atPosition( Position aPos ) {
		return atPosition( aPos.getPositionX(), aPos.getPositionY() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P atPosition( int aPosX, int aPosY ) {
		final Position thePos = toAbsPos( aPosX, aPosY );
		final Map<Integer, P> theColumn = _rowToColumn.get( thePos.getPositionX() );
		if ( theColumn != null ) {
			return theColumn.get( thePos.getPositionY() );
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasAtTopOf( Position aPos ) {
		final int x = aPos.getPositionX();
		final int y = toTopYPosition( aPos );
		return hasAtPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P atTopOf( Position aPos ) {
		final int x = aPos.getPositionX();
		final int y = toTopYPosition( aPos );
		return atPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasAtTopRightOf( Position aPos ) {
		final int y = toTopYPosition( aPos );
		final int x = toRightXPosition( aPos );
		return hasAtPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P atTopRightOf( Position aPos ) {
		final int y = toTopYPosition( aPos );
		final int x = toRightXPosition( aPos );
		return atPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasAtRightOf( Position aPos ) {
		final int y = aPos.getPositionY();
		final int x = toRightXPosition( aPos );
		return hasAtPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P atRightOf( Position aPos ) {
		final int y = aPos.getPositionY();
		final int x = toRightXPosition( aPos );
		return atPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasAtBottomRightOf( Position aPos ) {
		final int y = toBottomYPosition( aPos );
		final int x = toRightXPosition( aPos );
		return hasAtPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P atBottomRightOf( Position aPos ) {
		final int y = toBottomYPosition( aPos );
		final int x = toRightXPosition( aPos );
		return atPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasAtBottomOf( Position aPos ) {
		final int x = aPos.getPositionX();
		final int y = toBottomYPosition( aPos );
		return hasAtPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P atBottomOf( Position aPos ) {
		final int x = aPos.getPositionX();
		final int y = toBottomYPosition( aPos );
		return atPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasAtBottomLeftOf( Position aPos ) {
		final int y = toBottomYPosition( aPos );
		final int x = toLeftXPosition( aPos );
		return hasAtPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P atBottomLeftOf( Position aPos ) {
		final int y = toBottomYPosition( aPos );
		final int x = toLeftXPosition( aPos );
		return atPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasAtLeftOf( Position aPos ) {
		final int y = aPos.getPositionY();
		final int x = toLeftXPosition( aPos );
		return hasAtPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P atLeftOf( Position aPos ) {
		final int y = aPos.getPositionY();
		final int x = toLeftXPosition( aPos );
		return atPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasAtTopLeftOf( Position aPos ) {
		final int y = toTopYPosition( aPos );
		final int x = toLeftXPosition( aPos );
		return hasAtPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P atTopLeftOf( Position aPos ) {
		final int y = toTopYPosition( aPos );
		final int x = toLeftXPosition( aPos );
		return atPosition( x, y );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<Integer, P> getRow( int aRow ) {
		return _rowToColumn.get( aRow );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<Integer, P> getColumn( int aColumn ) {
		return _columnToRow.get( aColumn );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GridMode getGridMode() {
		return _gridMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridMode( GridMode aGridMode ) {
		if ( aGridMode != _gridMode ) {
			final GridModeChangedEvent<P, S> theEvent = new GridModeChangedEvent<>( aGridMode, _gridMode, this );
			_gridMode = aGridMode;
			try {
				fireEvent( theEvent, ExecutionStrategy.SEQUENTIAL );
			}
			catch ( VetoException ignore ) {}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<P> getPlayers() {
		return _players;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P putPlayer( P aPlayer ) {
		_players.add( aPlayer );
		final P thePrevPlayer = atPosition( aPlayer );
		if ( thePrevPlayer != null ) {
			if ( !removePlayer( thePrevPlayer ) ) {
				throw new IllegalStateException( "Illegal state detected while replacing player \"" + thePrevPlayer + "> from position (" + thePrevPlayer.getPositionX() + ":" + aPlayer.getPositionY() + ") with player player \"" + aPlayer + "> from position (" + aPlayer.getPositionX() + ":" + aPlayer.getPositionY() + "). Probably the player's coordinates changed while removing." );
			}
		}
		if ( addToLookup( aPlayer ) != null ) {
			throw new IllegalStateException( "Illegal state detected while removing player \"" + aPlayer + "> from position (" + aPlayer.getPositionX() + ":" + aPlayer.getPositionY() + "). Probably the player's coordinates changed while removing." );
		}
		final PlayerAddedEvent<P, S> theEvent = new PlayerAddedEvent<>( aPlayer, this );
		aPlayer.subscribeObserver( _observer );

		try {
			fireEvent( theEvent, ExecutionStrategy.SEQUENTIAL );
		}
		catch ( VetoException ignore ) {}
		return thePrevPlayer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removePlayer( P aPlayer ) {
		if ( _players.remove( aPlayer ) ) {
			if ( removeFromLookup( aPlayer ) != aPlayer ) {
				throw new IllegalStateException( "Illegal state detected while removing player \"" + aPlayer + "> from position (" + aPlayer.getPositionX() + ":" + aPlayer.getPositionY() + "). Probably the player's coordinates changed while removing." );
			}
			final PlayerRemovedEvent<P, S> theEvent = new PlayerRemovedEvent<>( aPlayer, this );
			aPlayer.unsubscribeObserver( _observer );
			aPlayer.subscribeObserver( _observer );
			try {
				fireEvent( theEvent, ExecutionStrategy.SEQUENTIAL );
			}
			catch ( VetoException ignore ) {}
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clearPlayers() {
		_players.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int playerCount() {
		return _players.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPlayers() {
		return !_players.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<P> players() {
		return _players.iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPlayer( P aPlayer ) {
		return _players.contains( aPlayer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGridWidth() {
		return _gridWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridDimension( int aWidth, int aHeight ) {
		if ( aWidth != _gridWidth || aHeight != _gridHeight ) {
			final GridDimensionChangedEvent<P, S> theEvent = new GridDimensionChangedEvent<>( aWidth, aHeight, _gridWidth, _gridHeight, this );
			_gridWidth = aWidth;
			_gridHeight = aHeight;
			try {
				fireEvent( theEvent, ExecutionStrategy.SEQUENTIAL );
			}
			catch ( VetoException ignore ) {}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridDimension( GridDimension aDimension ) {
		setGridDimension( aDimension.getGridWidth(), aDimension.getGridHeight() );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridDimension( Dimension aDimension ) {
		setGridDimension( aDimension.getWidth(), aDimension.getHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridWidth( int aWidth ) {
		setGridDimension( aWidth, _gridHeight );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridHeight( int aHeight ) {
		setGridDimension( _gridWidth, aHeight );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGridHeight() {
		return _gridHeight;
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean subscribeObserver( CheckerboardObserver<P, S> aObserver ) {
		if ( super.subscribeObserver( aObserver ) ) {
			try {
				fireEvent( new SubscribeEvent<Checkerboard<P, S>>( this ), ExecutionStrategy.SEQUENTIAL );
			}
			catch ( VetoException ignore ) {}
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean unsubscribeObserver( CheckerboardObserver<P, S> aObserver ) {
		try {
			fireEvent( new UnsubscribeEvent<Checkerboard<P, S>>( this ), ExecutionStrategy.SEQUENTIAL );
		}
		catch ( VetoException ignore ) {}
		return super.unsubscribeObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _players == null ) ? 0 : _players.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final AbstractCheckerboard<?, ?, ?> other = (AbstractCheckerboard<?, ?, ?>) obj;
		if ( _players == null ) {
			if ( other._players != null ) {
				return false;
			}
		}
		else if ( !_players.equals( other._players ) ) {
			return false;
		}
		return true;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Fire event.
	 *
	 * @param aEvent the event
	 * @param aObserver the observer
	 * @param aExecutionStrategy the execution strategy
	 * 
	 * @return true, if successful
	 * 
	 * @throws VetoException the veto exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected boolean fireEvent( Event<?> aEvent, CheckerboardObserver<P, S> aObserver, ExecutionStrategy aExecutionStrategy ) throws VetoException {
		if ( aEvent instanceof SubscribeEvent ) {
			aObserver.onSubscribe( (SubscribeEvent<Checkerboard<P, S>>) aEvent );
		}
		if ( aEvent instanceof UnsubscribeEvent ) {
			aObserver.onUnsubscribe( (UnsubscribeEvent<Checkerboard<P, S>>) aEvent );
		}
		if ( aEvent instanceof CheckerboardEvent<?, ?> ) {
			aObserver.onCheckerboardEvent( (CheckerboardEvent<P, S>) aEvent );
			if ( aEvent instanceof PlayerAddedEvent ) {
				aObserver.onPlayerAddedEvent( (PlayerAddedEvent<P, S>) aEvent );
			}
			if ( aEvent instanceof PlayerRemovedEvent ) {
				aObserver.onPlayerRemovedEvent( (PlayerRemovedEvent<P, S>) aEvent );
			}
			if ( aEvent instanceof GridModeChangedEvent ) {
				aObserver.onGridModeChangedEvent( (GridModeChangedEvent<P, S>) aEvent );
			}
			if ( aEvent instanceof ViewportDimensionChangedEvent ) {
				aObserver.onViewportDimensionChangedEvent( (ViewportDimensionChangedEvent<P, S>) aEvent );
			}
			if ( aEvent instanceof ViewportOffsetChangedEvent ) {
				aObserver.onViewportOffsetChangedEvent( (ViewportOffsetChangedEvent<P, S>) aEvent );
			}
		}
		else if ( aEvent instanceof PlayerEvent<?> ) {
			aObserver.onPlayerEvent( (PlayerEvent<P>) aEvent, this );
			if ( aEvent instanceof ChangePlayerPositionEvent ) {
				aObserver.onChangePlayerPositionEvent( (ChangePlayerPositionEvent<P>) aEvent, this );
			}
			if ( aEvent instanceof PlayerPositionChangedEvent ) {
				aObserver.onPlayerPositionChangedEvent( (PlayerPositionChangedEvent<P>) aEvent, this );
			}
			if ( aEvent instanceof PlayerStateChangedEvent ) {
				aObserver.onPlayerStateChangedEvent( (PlayerStateChangedEvent<P, S>) aEvent, this );
			}
			if ( aEvent instanceof PlayerVisibilityChangedEvent ) {
				aObserver.onPlayerVisibilityChangedEvent( (PlayerVisibilityChangedEvent<P>) aEvent, this );
			}
			if ( aEvent instanceof PlayerDraggabilityChangedEvent ) {
				aObserver.onPlayerDraggabilityChangedEvent( (PlayerDraggabilityChangedEvent<P>) aEvent, this );
			}
		}
		return true;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Calculates the absolute position as of the {@link GridMode}. In case the
	 * {@link GridMode} is set to {@link GridMode#PERIODIC} and the positions
	 * exceed the grid dimension, then the positions are reckoned to be a
	 * multiple of the grid dimensions and relocated accordingly.
	 * 
	 * @param aPosX The x-position.
	 * @param aPosX The y-position.
	 * 
	 * @return The position as of the {@link GridMode}.
	 */
	private Position toAbsPos( int aPosX, int aPosY ) {
		if ( getGridMode() == GridMode.PERIODIC ) {
			while ( aPosX < 0 ) { // Modulo?
				aPosX += getGridWidth();
			}
			while ( aPosY < 0 ) { // Modulo?
				aPosY += getGridHeight();
			}

			while ( aPosX >= getGridWidth() ) { // Modulo?
				aPosX -= getGridWidth();
			}

			while ( aPosY >= getGridHeight() ) { // Modulo?
				aPosY -= getGridHeight();
			}

		}
		final Position thePos = new PositionImpl( aPosX, aPosY );
		return thePos;
	}

	/**
	 * Removes the from lookup.
	 *
	 * @param aPosition the position
	 * 
	 * @return the p
	 */
	private synchronized P removeFromLookup( Position aPosition ) {
		P removeFromRow = null;
		final Map<Integer, P> theColumn = _rowToColumn.get( aPosition.getPositionX() );
		if ( theColumn != null ) {
			removeFromRow = theColumn.remove( aPosition.getPositionY() );
		}
		P removeFromColumn = null;
		final Map<Integer, P> theRow = _columnToRow.get( aPosition.getPositionY() );
		if ( theRow != null ) {
			removeFromColumn = theRow.remove( aPosition.getPositionX() );
		}
		if ( removeFromColumn != removeFromRow ) {
			throw new IllegalStateException( "Illegal state detected while removing player \"" + aPosition + "> from position (" + aPosition.getPositionX() + ":" + aPosition.getPositionY() + "). Probably the player's coordinates changed while removing." );
		}
		return removeFromColumn;
	}

	/**
	 * Adds the to lookup.
	 *
	 * @param aPlayer the player
	 * 
	 * @return the p
	 */
	private synchronized P addToLookup( P aPlayer ) {
		P theAddFromRow = null;
		Map<Integer, P> theColumn = _rowToColumn.get( aPlayer.getPositionX() );
		if ( theColumn == null ) {
			theColumn = new HashMap<>();
			_rowToColumn.put( aPlayer.getPositionX(), theColumn );
		}
		theAddFromRow = theColumn.put( aPlayer.getPositionY(), aPlayer );

		P theAddFromColumn = null;
		Map<Integer, P> theRow = _columnToRow.get( aPlayer.getPositionY() );
		if ( theRow == null ) {
			theRow = new HashMap<>();
			_columnToRow.put( aPlayer.getPositionY(), theRow );
		}
		theAddFromColumn = theRow.put( aPlayer.getPositionX(), aPlayer );

		if ( theAddFromColumn != theAddFromRow ) {
			throw new IllegalStateException( "Illegal state detected while removing player \"" + aPlayer + "> from position (" + aPlayer.getPositionX() + ":" + aPlayer.getPositionY() + "). Probably the player's coordinates changed while removing." );
		}
		return theAddFromColumn;
	}

	/**
	 * Update player.
	 *
	 * @param aPlayer the the player
	 * @param aPrecedingPosition the the preceding position
	 * 
	 * @return the p
	 */
	private P updatePlayer( P aPlayer, Position aPrecedingPosition ) {
		final P theReplacedPlayer = atPosition( aPlayer );
		removePlayer( theReplacedPlayer );
		final P theTmpPlayer = addToLookup( aPlayer );
		if ( theTmpPlayer != null ) {
			throw new IllegalStateException( "Illegal state detected while moving player \"" + aPlayer + "> from position (" + aPrecedingPosition.toString() + "): The target position (although being cleared before) now contains another player \"" + theTmpPlayer + ">. Probably some thread race condition and insufficient synchronization." );
		}
		final P thePrevPlayerPos = removeFromLookup( aPrecedingPosition );
		if ( thePrevPlayerPos != aPlayer ) {
			throw new IllegalStateException( "Illegal state detected while moving player \"" + aPlayer + "> from position (" + aPrecedingPosition.toString() + "): The previous position contains the wrong player \"" + thePrevPlayerPos + "> instead of the expected player \"" + aPlayer + "\". Probably some thread race condition and insufficient synchronization." );
		}
		return theReplacedPlayer;
	}

	/**
	 * To top Y position.
	 *
	 * @param aPos the pos
	 * 
	 * @return the int
	 */
	private int toTopYPosition( Position aPos ) {
		int y = aPos.getPositionY() - 1;
		if ( y < 0 ) {
			if ( getGridMode() == GridMode.PERIODIC ) {
				y = getGridHeight() - 1;
			}
			else {
				throw new IndexOutOfBoundsException( "As the grid is in mode <" + getGridMode() + "> an index of <" + aPos.toString() + "> is out of bounds!" );
			}
		}
		return y;
	}

	/**
	 * To right X position.
	 *
	 * @param aPos the pos
	 * 
	 * @return the int
	 */
	private int toRightXPosition( Position aPos ) {
		int x = aPos.getPositionX() + 1;
		if ( x > getGridWidth() - 1 ) {
			if ( getGridMode() == GridMode.PERIODIC ) {
				x = 0;
			}
			else {
				throw new IndexOutOfBoundsException( "As the grid is in mode <" + getGridMode() + "> an index of <" + aPos.toString() + "> is out of bounds!" );
			}
		}
		return x;
	}

	/**
	 * To left Y position.
	 *
	 * @param aPos the pos
	 * 
	 * @return the int
	 */
	private int toBottomYPosition( Position aPos ) {
		int y = aPos.getPositionY() + 1;
		if ( y > getGridHeight() - 1 ) {
			if ( getGridMode() == GridMode.PERIODIC ) {
				y = 0;
			}
			else {
				throw new IndexOutOfBoundsException( "As the grid is in mode <" + getGridMode() + "> an index of <" + aPos.toString() + "> is out of bounds!" );
			}
		}
		return y;
	}

	/**
	 * To left X position.
	 *
	 * @param aPos the pos
	 * 
	 * @return the int
	 */
	private int toLeftXPosition( Position aPos ) {
		int x = aPos.getPositionX() - 1;
		if ( x < 0 ) {
			if ( getGridMode() == GridMode.PERIODIC ) {
				x = getGridWidth() - 1;
			}
			else {
				throw new IndexOutOfBoundsException( "As the grid is in mode <" + getGridMode() + "> an index of <" + aPos.toString() + "> is out of bounds!" );
			}
		}
		return x;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Class CheckerboardPlayerObserverImpl.
	 */
	private class CheckerboardPlayerObserver implements PlayerObserver<P, S> {
		@Override
		public void onPlayerEvent( PlayerEvent<P> aPlayerEvent ) {
			try {
				fireEvent( aPlayerEvent, ExecutionStrategy.SEQUENTIAL );
			}
			catch ( VetoException ignore ) {}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void onChangePlayerPositionEvent( ChangePlayerPositionEvent<P> aPlayerEvent ) throws VetoException {
			try {
				fireEvent( aPlayerEvent, ExecutionStrategy.SEQUENTIAL );
			}
			catch ( VetoException ignore ) {}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void onPlayerPositionChangedEvent( PlayerPositionChangedEvent<P> aPlayerEvent ) {
			final P thePlayer = aPlayerEvent.getSource();
			final Position thePrecedingPosition = aPlayerEvent.getPrecedingPosition();
			updatePlayer( thePlayer, thePrecedingPosition );

			try {
				fireEvent( aPlayerEvent, ExecutionStrategy.SEQUENTIAL );
			}
			catch ( VetoException ignore ) {}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void onPlayerStateChangedEvent( PlayerStateChangedEvent<P, S> aPlayerEvent ) {
			try {
				fireEvent( aPlayerEvent, ExecutionStrategy.SEQUENTIAL );
			}
			catch ( VetoException ignore ) {}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void onPlayerVisibilityChangedEvent( PlayerVisibilityChangedEvent<P> aPlayerEvent ) {
			try {
				fireEvent( aPlayerEvent, ExecutionStrategy.SEQUENTIAL );
			}
			catch ( VetoException ignore ) {}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void onPlayerDraggabilityChangedEvent( PlayerDraggabilityChangedEvent<P> aPlayerEvent ) {
			try {
				fireEvent( aPlayerEvent, ExecutionStrategy.SEQUENTIAL );
			}
			catch ( VetoException ignore ) {}
		}
	}
}
