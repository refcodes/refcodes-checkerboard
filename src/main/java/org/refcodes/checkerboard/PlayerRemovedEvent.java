package org.refcodes.checkerboard;

import org.refcodes.checkerboard.AbstractCheckerboardEvent.AbstractPlayerCheckerboardEvent;

/**
 * The Class PlayerRemovedEvent.
 *
 * @param <P> the generic type
 * @param <S> the generic type
 */
public class PlayerRemovedEvent<P extends Player<P, S>, S> extends AbstractPlayerCheckerboardEvent<P, S> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final CheckerboardAction ACTION = CheckerboardAction.PLAYER_REMOVED;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new player removed event.
	 *
	 * @param aPlayer the player
	 * @param aSource The according source (origin).
	 */
	public PlayerRemovedEvent( P aPlayer, Checkerboard<P, S> aSource ) {
		super( ACTION, aPlayer, aSource );
	}
}
