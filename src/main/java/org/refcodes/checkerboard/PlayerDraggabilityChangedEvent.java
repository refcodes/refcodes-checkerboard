package org.refcodes.checkerboard;

/**
 * The Class PlayerDraggabilityChangedEvent.
 *
 * @param <P> the generic type
 */
public class PlayerDraggabilityChangedEvent<P extends Player<P, ?>> extends AbstractPlayerEvent<P> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final PlayerAction ACTION = PlayerAction.DRAGGABILITY_CHANGED;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new draggability changed event.
	 *
	 * @param aSource The according source (origin).
	 */
	public PlayerDraggabilityChangedEvent( P aSource ) {
		super( ACTION, aSource );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return super.toString() + ", draggability := " + getSource().isDraggable() + " (state before: " + ( !getSource().isDraggable() ) + ")";
	}
}
