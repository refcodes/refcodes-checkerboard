package org.refcodes.checkerboard;

import org.refcodes.graphical.Position;
import org.refcodes.graphical.PositionImpl;
import org.refcodes.graphical.Vector;

/**
 * The Class ChangePlayerPositionEvent.
 *
 * @param <P> the generic type
 */
public class ChangePlayerPositionEvent<P extends Player<P, ?>> extends AbstractPlayerEvent<P> implements Position, Vector {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final PlayerAction ACTION = PlayerAction.CHANGE_POSITION;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final int _posX;

	private final int _posY;

	private final Position _precedingPosition;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new change position event.
	 *
	 * @param aPosX the pos X
	 * @param aPosY the pos Y
	 * @param aPrePosX the pre pos X
	 * @param aPrePosY the pre pos Y
	 * @param aSource The according source (origin).
	 */
	public ChangePlayerPositionEvent( int aPosX, int aPosY, int aPrePosX, int aPrePosY, P aSource ) {
		super( ACTION, aSource );
		_posX = aPosX;
		_posY = aPosY;
		_precedingPosition = new PositionImpl( aPrePosX, aPrePosY );
	}

	/**
	 * Instantiates a new change position event.
	 *
	 * @param aPosition the position
	 * @param aPrecedingPosition the preceding position
	 * @param aSource The according source (origin).
	 */
	public ChangePlayerPositionEvent( Position aPosition, Position aPrecedingPosition, P aSource ) {
		super( ACTION, aSource );
		_posX = aPosition.getPositionX();
		_posY = aPosition.getPositionY();
		_precedingPosition = aPrecedingPosition;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Gets the preceding position.
	 *
	 * @return the preceding position
	 */
	public Position getPrecedingPosition() {
		return _precedingPosition;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPositionX() {
		return _posX;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPositionY() {
		return _posY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getVectorX() {
		return _posX - _precedingPosition.getPositionX();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getVectorY() {
		return _posY - _precedingPosition.getPositionY();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return super.toString() + ", x := " + _posX + ", y := " + _posY + " (state before: " + _precedingPosition + ")";
	}
}
