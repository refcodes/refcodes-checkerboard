// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

/**
 * Provides an accessor for a background factory property.
 *
 * @param <BF> the generic type of the according {@link BackgroundFactory}.
 */
public interface BackgroundFactoryAccessor<BF extends BackgroundFactory<?>> {

	/**
	 * Retrieves the background factory from the background factory property.
	 * 
	 * @return The background factory stored by the background factory property.
	 */
	BF getBackgroundFactory();

	/**
	 * Provides a mutator for a background factory property.
	 *
	 * @param <BF> the generic type
	 */
	public interface BackgroundFactoryMutator<BF extends BackgroundFactory<?>> {

		/**
		 * Sets the background factory for the background factory property.
		 * 
		 * @param aBackgroundFactory The background factory to be stored by the
		 *        background factory property.
		 */
		void setBackgroundFactory( BF aBackgroundFactory );
	}

	/**
	 * Provides a builder method for a background factory property returning the
	 * builder for applying multiple build operations.
	 *
	 * @param <BF> the generic type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BackgroundFactoryBuilder<BF extends BackgroundFactory<?>, B extends BackgroundFactoryBuilder<BF, B>> {

		/**
		 * Sets the background factory for the background factory property.
		 * 
		 * @param aBackgroundFactory The background factory to be stored by the
		 *        background factory property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBackgroundFactory( BF aBackgroundFactory );
	}

	/**
	 * Provides a background factory property.
	 *
	 * @param <BF> the generic type
	 */
	public interface BackgroundFactoryProperty<BF extends BackgroundFactory<?>> extends BackgroundFactoryAccessor<BF>, BackgroundFactoryMutator<BF> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link BackgroundFactory} (setter) as of
		 * {@link #setBackgroundFactory(BackgroundFactory)} and returns the very
		 * same value (getter).
		 * 
		 * @param aBackgroundFactory The {@link BackgroundFactory} to set (via
		 *        {@link #setBackgroundFactory(BackgroundFactory)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default BF letBackgroundFactory( BF aBackgroundFactory ) {
			setBackgroundFactory( aBackgroundFactory );
			return aBackgroundFactory;
		}
	}
}
