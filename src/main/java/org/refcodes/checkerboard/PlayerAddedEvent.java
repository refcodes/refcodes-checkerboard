package org.refcodes.checkerboard;

import org.refcodes.checkerboard.AbstractCheckerboardEvent.AbstractPlayerCheckerboardEvent;

/**
 * The Class PlayerAddedEvent.
 *
 * @param <P> the generic type
 * @param <S> the generic type
 */
public class PlayerAddedEvent<P extends Player<P, S>, S> extends AbstractPlayerCheckerboardEvent<P, S> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final CheckerboardAction ACTION = CheckerboardAction.PLAYER_ADDED;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new player added event.
	 *
	 * @param aPlayer the player
	 * @param aSource The according source (origin).
	 */
	public PlayerAddedEvent( P aPlayer, Checkerboard<P, S> aSource ) {
		super( ACTION, aPlayer, aSource );
	}
}
