package org.refcodes.checkerboard;

/**
 * The Enum CheckerboardAction.
 */
public enum CheckerboardAction {

	PLAYER_ADDED,

	PLAYER_REMOVED,

	GRID_MODE_CHANGED,

	GRID_DIMENSION_CHANGED,

	GRID_POSITION_CLICKED,

	INITIALIZE
}
