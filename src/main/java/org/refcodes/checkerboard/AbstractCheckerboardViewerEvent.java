package org.refcodes.checkerboard;

/**
 * The Class AbstractCheckerboardViewerEvent.
 *
 * @param <P> the generic type
 * @param <S> the generic type
 */
public abstract class AbstractCheckerboardViewerEvent<P extends Player<P, S>, S> implements CheckerboardViewerEvent<P, S> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final CheckerboardViewer<P, S, ?> _source;

	private final CheckerboardViewerAction _action;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new abstract checkerboard viewer event.
	 *
	 * @param aAction the action
	 * @param aSource The according source (origin).
	 */
	public AbstractCheckerboardViewerEvent( CheckerboardViewerAction aAction, CheckerboardViewer<P, S, ?> aSource ) {
		_source = aSource;
		_action = aAction;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CheckerboardViewerAction getAction() {
		return _action;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CheckerboardViewer<P, S, ?> getSource() {
		return _source;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "action := " + _action;

	}
}
