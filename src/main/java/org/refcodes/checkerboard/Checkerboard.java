package org.refcodes.checkerboard;

import java.util.Map;
import java.util.function.Consumer;

import org.refcodes.component.Component;
import org.refcodes.graphical.GridDimension.GridDimensionBuilder;
import org.refcodes.graphical.GridDimension.GridDimensionProperty;
import org.refcodes.graphical.GridModeAccessor.GridModeBuilder;
import org.refcodes.graphical.GridModeAccessor.GridModeProperty;
import org.refcodes.graphical.Position;
import org.refcodes.observer.Observable;

/**
 * The Interface Checkerboard.
 *
 * @param <P> the generic type
 * @param <S> the generic type
 */
public interface Checkerboard<P extends Player<P, S>, S> extends Players<P>, Observable<CheckerboardObserver<P, S>>, GridDimensionProperty, GridDimensionBuilder<Checkerboard<P, S>>, GridModeProperty, GridModeBuilder<Checkerboard<P, S>>, Component {

	/**
	 * Checks for at position.
	 *
	 * @param aPos the pos
	 * 
	 * @return true, if successful
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	boolean hasAtPosition( Position aPos );

	/**
	 * Checks for at position.
	 *
	 * @param aPosX the pos X
	 * @param aPosY the pos Y
	 * 
	 * @return true, if successful
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	boolean hasAtPosition( int aPosX, int aPosY );

	/**
	 * At position.
	 *
	 * @param aPos the pos
	 * 
	 * @return the p
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	P atPosition( Position aPos );

	/**
	 * At position.
	 *
	 * @param aPosX the pos X
	 * @param aPosY the pos Y
	 * 
	 * @return the p
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	P atPosition( int aPosX, int aPosY );

	/**
	 * Gets the row.
	 *
	 * @param aRow the row
	 * 
	 * @return the row
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	Map<Integer, P> getRow( int aRow );

	/**
	 * Gets the column.
	 *
	 * @param aColumn the column
	 * 
	 * @return the column
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	Map<Integer, P> getColumn( int aColumn );

	/**
	 * Checks for at top of.
	 *
	 * @param aPos the pos
	 * 
	 * @return true, if successful
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	boolean hasAtTopOf( Position aPos );

	/**
	 * At top of.
	 *
	 * @param aPos the pos
	 * 
	 * @return the p
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	P atTopOf( Position aPos );

	/**
	 * Checks for at top right of.
	 *
	 * @param aPos the pos
	 * 
	 * @return true, if successful
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	boolean hasAtTopRightOf( Position aPos );

	/**
	 * At top right of.
	 *
	 * @param aPos the pos
	 * 
	 * @return the p
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	P atTopRightOf( Position aPos );

	/**
	 * Checks for at right of.
	 *
	 * @param aPos the pos
	 * 
	 * @return true, if successful
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	boolean hasAtRightOf( Position aPos );

	/**
	 * At right of.
	 *
	 * @param aPos the pos
	 * 
	 * @return the p
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	P atRightOf( Position aPos );

	/**
	 * Checks for at bottom right of.
	 *
	 * @param aPos the pos
	 * 
	 * @return true, if successful
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	boolean hasAtBottomRightOf( Position aPos );

	/**
	 * At bottom right of.
	 *
	 * @param aPos the pos
	 * 
	 * @return the p
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	P atBottomRightOf( Position aPos );

	/**
	 * Checks for at bottom of.
	 *
	 * @param aPos the pos
	 * 
	 * @return true, if successful
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	boolean hasAtBottomOf( Position aPos );

	/**
	 * At bottom of.
	 *
	 * @param aPos the pos
	 * 
	 * @return the p
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	P atBottomOf( Position aPos );

	/**
	 * Checks for at bottom left of.
	 *
	 * @param aPos the pos
	 * 
	 * @return true, if successful
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	boolean hasAtBottomLeftOf( Position aPos );

	/**
	 * At bottom left of.
	 *
	 * @param aPos the pos
	 * 
	 * @return the p
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	P atBottomLeftOf( Position aPos );

	/**
	 * Checks for at left of.
	 *
	 * @param aPos the pos
	 * 
	 * @return true, if successful
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	boolean hasAtLeftOf( Position aPos );

	/**
	 * At left of.
	 *
	 * @param aPos the pos
	 * 
	 * @return the p
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	P atLeftOf( Position aPos );

	/**
	 * Checks for at top left of.
	 *
	 * @param aPos the pos
	 * 
	 * @return true, if successful
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	boolean hasAtTopLeftOf( Position aPos );

	/**
	 * At top left of.
	 *
	 * @param aPos the pos
	 * 
	 * @return the p
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	P atTopLeftOf( Position aPos );

	/**
	 * Performs the given action for each {@link Player} on the
	 * {@link Checkerboard} until all players have been processed or the action
	 * throws an exception.
	 * 
	 * @param aConsumer The action to be performed for each {@link Player}.
	 */
	void forEach( Consumer<P> aConsumer );

}
