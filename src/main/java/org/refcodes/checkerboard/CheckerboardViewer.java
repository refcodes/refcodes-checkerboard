package org.refcodes.checkerboard;

import org.refcodes.component.Initializable.InitializeBuilder;
import org.refcodes.component.InitializableComponent;
import org.refcodes.graphical.GridDimension;
import org.refcodes.graphical.GridModeAccessor;
import org.refcodes.graphical.MinViewportDimension.MinViewportDimensionBuilder;
import org.refcodes.graphical.MinViewportDimension.MinViewportDimensionProperty;
import org.refcodes.graphical.Viewport;
import org.refcodes.graphical.ViewportDimension.ViewportDimensionBuilder;
import org.refcodes.graphical.ViewportDimension.ViewportDimensionProperty;
import org.refcodes.graphical.ViewportOffset.ViewportOffsetBuilder;
import org.refcodes.graphical.ViewportOffset.ViewportOffsetProperty;
import org.refcodes.graphical.ViewportWidthAccessor.ViewportWidthBuilder;
import org.refcodes.graphical.ViewportWidthAccessor.ViewportWidthProperty;
import org.refcodes.mixin.Reconcilable;

/**
 * The Interface CheckerboardViewer.
 *
 * @param <P> The type representing a {@link Player}
 * @param <S> The type which's instances represent a {@link Player} state.
 * @param <CBV> The {@link CheckerboardViewer}'s type implementing this
 *        interface.
 */
public interface CheckerboardViewer<P extends Player<P, S>, S, CBV extends CheckerboardViewer<P, S, CBV>> extends CheckerboardObserver<P, S>, Viewport, ViewportDimensionProperty, ViewportDimensionBuilder<CBV>, MinViewportDimensionProperty, MinViewportDimensionBuilder<CBV>, ViewportWidthProperty, ViewportWidthBuilder<CBV>, ViewportOffsetProperty, ViewportOffsetBuilder<CBV>, GridDimension, GridModeAccessor, InitializableComponent, InitializeBuilder<CBV>, Reconcilable {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void centerViewportOffset( int aPositionX, int aPositionY ) {
		centerViewportOffset( aPositionX, aPositionY, getViewportWidth(), getViewportHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void centerViewportOffset( int aPositionX, int aPositionY, int aViewportWidth, int aViewportHeight ) {
		final int theGridWidth = getGridWidth();
		final int theGridHeight = getGridHeight();
		int theOffsetX = aPositionX - ( aViewportWidth / 2 );
		int theOffsetY = aPositionY - ( aViewportHeight / 2 );
		if ( theOffsetX > theGridWidth - aViewportWidth ) {
			theOffsetX = theGridWidth - aViewportWidth;
		}
		if ( theOffsetY > theGridHeight - aViewportHeight ) {
			theOffsetY = theGridHeight - aViewportHeight;
		}
		if ( theOffsetX < 0 ) {
			theOffsetX = 0;
		}
		if ( theOffsetY < 0 ) {
			theOffsetY = 0;
		}
		setViewportOffset( -theOffsetX, -theOffsetY );
	}

	// void reconcile();
}