// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

/**
 * Provides an accessor for a player property.
 *
 * @param <P> the generic type
 * @param <S> the generic type
 */
public interface CheckerboardAccessor<P extends Player<P, S>, S> {

	/**
	 * Retrieves the player from the player property.
	 * 
	 * @return The player stored by the player property.
	 */
	Checkerboard<P, S> getCheckerboard();

	/**
	 * Provides a mutator for a player property.
	 *
	 * @param <P> the generic type
	 * @param <S> the generic type
	 */
	public interface CheckerboardMutator<P extends Player<P, S>, S> {

		/**
		 * Sets the player for the player property.
		 * 
		 * @param aCheckerboard The player to be stored by the player property.
		 */
		void setCheckerboard( Checkerboard<P, S> aCheckerboard );
	}

	/**
	 * Provides a builder method for a player property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <P> the generic type
	 * @param <S> the generic type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CheckerboardBuilder<P extends Player<P, S>, S, B extends CheckerboardBuilder<P, S, B>> {

		/**
		 * Sets the player for the player property.
		 * 
		 * @param aCheckerboard The player to be stored by the player property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCheckerboard( Checkerboard<P, S> aCheckerboard );
	}

	/**
	 * Provides a player property.
	 *
	 * @param <P> the generic type
	 * @param <S> the generic type
	 */
	public interface CheckerboardProperty<P extends Player<P, S>, S> extends CheckerboardAccessor<P, S>, CheckerboardMutator<P, S> {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Checkerboard}
		 * (setter) as of {@link #setCheckerboard(Checkerboard)} and returns the
		 * very same value (getter).
		 * 
		 * @param aCheckerboard The {@link Checkerboard} to set (via
		 *        {@link #setCheckerboard(Checkerboard)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Checkerboard<P, S> letCheckerboard( Checkerboard<P, S> aCheckerboard ) {
			setCheckerboard( aCheckerboard );
			return aCheckerboard;
		}
	}
}
