// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

import org.refcodes.factory.ContextLookupFactory;

/**
 * A factory for creating Sprite objects.
 *
 * @author steiner
 * 
 * @param <IMG> the generic type of the "Sprite" being create. It may be of an
 *        image type or even of type {@link Character}, depending on the desired
 *        output device.
 * @param <S> the generic type of the status for which to generate a Sprite.
 * @param <CBV> the generic type for the actual {@link CheckerboardViewer} being
 *        used.
 */
public interface SpriteFactory<IMG, S, CBV extends CheckerboardViewer<?, S, ? extends CBV>> extends ContextLookupFactory<IMG, S, CBV> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

}
