package org.refcodes.checkerboard;

/**
 * The {@link PlayerClickedEvent} class.
 *
 * @param <P> the generic type
 */
public class PlayerClickedEvent<P extends Player<P, ?>> extends AbstractPlayerEvent<P> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final PlayerAction ACTION = PlayerAction.CLICKED;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new clicked event.
	 *
	 * @param aSource The according source (origin).
	 */
	public PlayerClickedEvent( P aSource ) {
		super( ACTION, aSource );
	}
}
