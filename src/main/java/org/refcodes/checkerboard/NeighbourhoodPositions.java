// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

import org.refcodes.graphical.Position;

/**
 * Returns the {@link Position} elements as of the according
 * {@link Neighbourhood}.
 */
public enum NeighbourhoodPositions {

	/**
	 * As of {@link VonNeumannNeighbourhood}.
	 */
	VON_NEUMANN(VonNeumannNeighbourhood.values()),

	/**
	 * As of {@link MooreNeighbourhood}.
	 */
	MOORE(MooreNeighbourhood.values());

	private Position[] _neigbours;

	/**
	 * Constructs the according poristions.
	 * 
	 * @param aNeigbours The {@link Position} elements which make up the
	 *        {@link NeighbourhoodPositions}.
	 */
	private NeighbourhoodPositions( Position[] aNeigbours ) {
		_neigbours = aNeigbours;
	}

	/**
	 * Returns the {@link Position} elements as of the according
	 * {@link Neighbourhood}.
	 * 
	 * @return The according {@link Position} elements.
	 */
	public Position[] getNeighbours() {
		return _neigbours;
	}
}
