package org.refcodes.checkerboard;

/**
 * The Class AbstractPlayerEvent.
 *
 * @param <P> the generic type
 */
public abstract class AbstractPlayerEvent<P extends Player<P, ?>> implements PlayerEvent<P> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final P _source;

	private final PlayerAction _action;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new abstract player event.
	 *
	 * @param aAction the action
	 * @param aSource The according source (origin).
	 */
	public AbstractPlayerEvent( PlayerAction aAction, P aSource ) {
		_action = aAction;
		_source = aSource;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PlayerAction getAction() {
		return _action;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P getSource() {
		return _source;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "action := " + _action;

	}
}