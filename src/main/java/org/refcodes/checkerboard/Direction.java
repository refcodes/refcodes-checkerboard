// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

import org.refcodes.graphical.Position;
import org.refcodes.graphical.PositionImpl;

/**
 * Definitions of the directions up, down, left and right.
 */
public enum Direction implements Neighbourhood<Direction> {

	LEFT(-1, 0), UP(0, -1), RIGHT(1, 0), DOWN(0, 1), NONE(0, 0);

	private int _posX;
	private int _posY;

	private Direction( int aPosX, int aPosY ) {
		_posX = aPosX;
		_posY = aPosY;
	}

	/**
	 * Returns the relative X position to position (0, 0).
	 * 
	 * @return The relative position X to the (0,0) coordinate.
	 */
	@Override
	public int getPositionX() {
		return _posX;
	}

	/**
	 * Returns the relative Y position to position (0, 0).
	 * 
	 * @return The relative position Y to the (0,0) coordinate.
	 */
	@Override
	public int getPositionY() {
		return _posY;
	}

	/**
	 * Returns the next clockwise state relative to the current state. Imagine
	 * the states as the positions on a clock, next to {@link #UP} would be
	 * {@link #RIGHT}, next to {@link #RIGHT} would be {@link #DOWN}, and so on
	 * (clockwise).
	 * 
	 * @return The next state relative to the current state.
	 */
	@Override
	public Direction clockwiseNext() {
		return switch ( this ) {
		case UP -> RIGHT;
		case RIGHT -> DOWN;
		case DOWN -> LEFT;
		case LEFT -> UP;
		case NONE -> NONE;
		};
	}

	/**
	 * Returns the next anti-clockwise state relative to the current state.
	 * Imagine the states as the positions on a clock, next to {@link #UP} would
	 * be {@link #LEFT}, next to {@link #LEFT} would be {@link #DOWN}, and so on
	 * (anti-clockwise).
	 * 
	 * @return The next state relative to the current state.
	 */
	@Override
	public Direction clockwisePrevious() {
		return switch ( this ) {
		case UP -> LEFT;
		case LEFT -> DOWN;
		case DOWN -> RIGHT;
		case RIGHT -> UP;
		case NONE -> NONE;
		};
	}

	/**
	 * Calculates the next {@link Position} for the provided {@link Position}
	 * regarding this {@link Direction}.
	 * 
	 * @param aPosition The {@link Position} for which to calculate the next
	 *        {@link Position}.
	 * 
	 * @return The next {@link Position}.
	 */
	public Position toNextPosition( Position aPosition ) {
		return new PositionImpl( aPosition.getPositionX() + _posX, aPosition.getPositionY() + _posY );
	}

	/**
	 * Calculates the next {@link Position} for the provided {@link Position}
	 * regarding this {@link Direction}.
	 * 
	 * @param aPositionX The X position for which to calculate the next
	 *        {@link Position}.
	 * 
	 * @param aPositionY The Y position for which to calculate the next
	 *        {@link Position}.
	 * 
	 * @return The next {@link Position}.
	 */
	public Position toNextPosition( int aPositionX, int aPositionY ) {
		return new PositionImpl( aPositionX + _posX, aPositionY + _posY );
	}
}
