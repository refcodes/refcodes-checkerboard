package org.refcodes.checkerboard;

import org.refcodes.graphical.ViewportDimension;
import org.refcodes.graphical.ViewportDimensionImpl;
import org.refcodes.graphical.ViewportOffset;
import org.refcodes.graphical.ViewportOffsetImpl;

/**
 * The Class ViewportDimensionChangedEvent.
 *
 * @param <P> the generic type
 * @param <S> the generic type
 */
public class ViewportDimensionChangedEvent<P extends Player<P, S>, S> extends AbstractCheckerboardViewerEvent<P, S> implements ViewportDimension {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final CheckerboardViewerAction ACTION = CheckerboardViewerAction.VIEWPORT_DIMENSION_CHANGED;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final int _height;
	private final int _width;
	private final ViewportDimension _precedingDimension;
	private final ViewportOffset _viewportOffset;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new viewport dimension changed event.
	 *
	 * @param aWidth the width
	 * @param aHeight the height
	 * @param aPrecedingWidth the preceding width
	 * @param aPrecedingHeigt the preceding heigt
	 * @param aOffsetX the offset X
	 * @param aOffsetY the offset Y
	 * @param aSource The according source (origin).
	 */
	public ViewportDimensionChangedEvent( int aWidth, int aHeight, int aPrecedingWidth, int aPrecedingHeigt, int aOffsetX, int aOffsetY, CheckerboardViewer<P, S, ?> aSource ) {
		super( ACTION, aSource );
		_width = aWidth;
		_height = aHeight;
		_precedingDimension = new ViewportDimensionImpl( aPrecedingWidth, aPrecedingHeigt );
		_viewportOffset = new ViewportOffsetImpl( aOffsetX, aOffsetY );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportHeight() {
		return _height;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportWidth() {
		return _width;
	}

	/**
	 * Gets the preceding viewport dimension.
	 *
	 * @return the preceding viewport dimension
	 */
	public ViewportDimension getPrecedingViewportDimension() {
		return _precedingDimension;
	}

	/**
	 * Gets the viewport offset.
	 *
	 * @return the viewport offset
	 */
	public ViewportOffset getViewportOffset() {
		return _viewportOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + _width + ":" + _height + ")@" + hashCode();
	}
}
