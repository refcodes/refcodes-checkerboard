// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

/**
 * Provides an accessor for a sprite factory property.
 *
 * @param <SF> the generic type
 */
public interface SpriteFactoryAccessor<SF extends SpriteFactory<?, ?, ?>> {

	/**
	 * Retrieves the sprite factory from the sprite factory property.
	 * 
	 * @return The sprite factory stored by the sprite factory property.
	 */
	SF getSpriteFactory();

	/**
	 * Provides a mutator for a sprite factory property.
	 *
	 * @param <SF> the generic type
	 */
	public interface SpriteFactoryMutator<SF extends SpriteFactory<?, ?, ?>> {

		/**
		 * Sets the sprite factory for the sprite factory property.
		 * 
		 * @param aSpriteFactory The sprite factory to be stored by the sprite
		 *        factory property.
		 */
		void setSpriteFactory( SF aSpriteFactory );
	}

	/**
	 * Provides a builder method for a sprite factory property returning the
	 * builder for applying multiple build operations.
	 *
	 * @param <SF> the generic type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SpriteFactoryBuilder<SF extends SpriteFactory<?, ?, ?>, B extends SpriteFactoryBuilder<SF, B>> {

		/**
		 * Sets the sprite factory for the sprite factory property.
		 * 
		 * @param aSpriteFactory The sprite factory to be stored by the sprite
		 *        factory property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSpriteFactory( SF aSpriteFactory );
	}

	/**
	 * Provides a sprite factory property.
	 *
	 * @param <SF> the generic type
	 */
	public interface SpriteFactoryProperty<SF extends SpriteFactory<?, ?, ?>> extends SpriteFactoryAccessor<SF>, SpriteFactoryMutator<SF> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link SpriteFactory}
		 * (setter) as of {@link #setSpriteFactory(SpriteFactory)} and returns
		 * the very same value (getter).
		 * 
		 * @param aSpriteFactory The {@link SpriteFactory} to set (via
		 *        {@link #setSpriteFactory(SpriteFactory)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SF letSpriteFactory( SF aSpriteFactory ) {
			setSpriteFactory( aSpriteFactory );
			return aSpriteFactory;
		}
	}
}
