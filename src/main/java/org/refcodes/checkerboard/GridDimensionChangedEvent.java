package org.refcodes.checkerboard;

import org.refcodes.graphical.GridDimension;
import org.refcodes.graphical.GridDimensionImpl;

/**
 * The {@link GridDimensionChangedEvent} event.
 *
 * @param <P> the {@link Player} generic type.
 * @param <S> The state of the player (dead, alive, strong, weak, king, queen,
 *        Pac-Man, ghost, fire, wood, water, strong queen, weak king, etc).
 */
public class GridDimensionChangedEvent<P extends Player<P, S>, S> extends AbstractCheckerboardEvent<P, S> implements GridDimension {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final CheckerboardAction ACTION = CheckerboardAction.GRID_DIMENSION_CHANGED;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final int _height;
	private final int _width;
	private final GridDimension _precedingDimension;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new grid dimension changed event.
	 *
	 * @param aWidth the width
	 * @param aHeight the height
	 * @param aPrecedingWidth the preceding width
	 * @param aPrecedingHeigt the preceding heigt
	 * @param aSource The according source (origin).
	 */
	public GridDimensionChangedEvent( int aWidth, int aHeight, int aPrecedingWidth, int aPrecedingHeigt, Checkerboard<P, S> aSource ) {
		super( ACTION, aSource );
		_width = aWidth;
		_height = aHeight;
		_precedingDimension = new GridDimensionImpl( aPrecedingWidth, aPrecedingHeigt );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGridHeight() {
		return _height;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGridWidth() {
		return _width;
	}

	/**
	 * Gets the preceding grid dimension.
	 *
	 * @return the preceding grid dimension
	 */
	public GridDimension getPrecedingGridDimension() {
		return _precedingDimension;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + _width + ":" + _height + ")@" + hashCode();
	}
}
