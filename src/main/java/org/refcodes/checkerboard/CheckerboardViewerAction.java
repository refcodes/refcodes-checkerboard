package org.refcodes.checkerboard;

/**
 * The Enum CheckerboardViewerAction.
 */
public enum CheckerboardViewerAction {

	VIEWPORT_DIMENSION_CHANGED,

	MIN_VIEWPORT_DIMENSION_CHANGED,

	FIELD_DIMENSION_CHANGED,

	VIEWPORT_OFFSET_CHANGED,

	INITIALIZE
}
