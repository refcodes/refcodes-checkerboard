// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

import org.refcodes.graphical.Position;

/**
 * Methods useful for working with neighborhoods related to cellular automatons,
 * such as the {@link VonNeumannNeighbourhood} or the
 * {@link MooreNeighbourhood}.
 * 
 * @param <S> The type representing the state.
 */
public interface Neighbourhood<S> extends Position {

	/**
	 * Returns the next clockwise state relative to the current state. Imagine
	 * the states as the positions on a clock, next to "12" would be "1", next
	 * to "1" would be "2", and so on (clockwise).
	 * 
	 * @return The next state relative to the current state.
	 */
	S clockwiseNext();

	/**
	 * Returns the next anti-clockwise state relative to the current state.
	 * Imagine the states as the positions on a clock, next to "12" would be
	 * "11", next to "11" would be "10", and so on (anti-clockwise).
	 * 
	 * @return The next state relative to the current state.
	 */
	S clockwisePrevious();

	/**
	 * Returns the next crosswise state relative to the current state. Imagine
	 * the states as the positions on a clock, next to "12" would be "6", next
	 * to "6" could be "9", next to "9" would be "3" and so on (crosswise).
	 * 
	 * @return The next state relative to the current state.
	 */
	// S crosswiseNext();

	/**
	 * Returns the next anti-crosswise state relative to the current state.
	 * Imagine the states as the positions on a clock, next to "12" would be
	 * "6", next to "6" could be "3", next to "3" would be "9" and so on
	 * (anti-crosswise).
	 * 
	 * @return The next state relative to the current state.
	 */
	// S crosswisePrevious();

	/**
	 * Returns the next neighborhood for the given rotation relative to the
	 * current state. Depending on the {@link Rotation} element provided, either
	 * {@link #clockwiseNext()} ({@link Rotation#CLOCKWISE}) or
	 * {@link #clockwisePrevious()} ({@link Rotation#ANTI_CLOCKWISE}) is called.
	 * 
	 * @param aRotation The rotation direction to use when calculating the next
	 *        neighborhood.
	 * 
	 * @return The next state relative to the current state.
	 */
	default S next( Rotation aRotation ) {
		return switch ( aRotation ) {
		case CLOCKWISE -> clockwiseNext();
		case ANTI_CLOCKWISE -> clockwisePrevious();
		};
	}
}
