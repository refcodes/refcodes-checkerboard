// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

/**
 * Definitions of the Von-Neumann neighbourhood.
 */
public enum VonNeumannNeighbourhood implements Neighbourhood<VonNeumannNeighbourhood> {

	LEFT(-1, 0), TOP(0, -1), RIGHT(1, 0), BOTTOM(0, 1);

	private int _posX;
	private int _posY;

	private VonNeumannNeighbourhood( int aPosX, int aPosY ) {
		_posX = aPosX;
		_posY = aPosY;
	}

	/**
	 * Returns the relative X position to position (0, 0).
	 * 
	 * @return The relative position X to the (0,0) coordinate.
	 */
	@Override
	public int getPositionX() {
		return _posX;
	}

	/**
	 * Returns the relative Y position to position (0, 0).
	 * 
	 * @return The relative position Y to the (0,0) coordinate.
	 */
	@Override
	public int getPositionY() {
		return _posY;
	}

	/**
	 * Returns the next clockwise state relative to the current state. Imagine
	 * the states as the positions on a clock, next to {@link #TOP} would be
	 * {@link #RIGHT}, next to {@link #RIGHT} would be {@link #BOTTOM}, and so
	 * on (clockwise).
	 * 
	 * @return The next state relative to the current state.
	 */
	@Override
	public VonNeumannNeighbourhood clockwiseNext() {
		return switch ( this ) {
		case TOP -> RIGHT;
		case RIGHT -> BOTTOM;
		case BOTTOM -> LEFT;
		case LEFT -> TOP;
		};
	}

	/**
	 * Returns the next anti-clockwise state relative to the current state.
	 * Imagine the states as the positions on a clock, next to {@link #TOP}
	 * would be {@link #LEFT}, next to {@link #LEFT} would be {@link #BOTTOM},
	 * and so on (anti-clockwise).
	 * 
	 * @return The next state relative to the current state.
	 */
	@Override
	public VonNeumannNeighbourhood clockwisePrevious() {
		return switch ( this ) {
		case TOP -> LEFT;
		case LEFT -> BOTTOM;
		case BOTTOM -> RIGHT;
		case RIGHT -> TOP;
		};
	}
}
