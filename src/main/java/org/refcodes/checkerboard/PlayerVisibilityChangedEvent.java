package org.refcodes.checkerboard;

/**
 * The Class PlayerVisibilityChangedEvent.
 *
 * @param <P> the generic type
 */
public class PlayerVisibilityChangedEvent<P extends Player<P, ?>> extends AbstractPlayerEvent<P> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final PlayerAction ACTION = PlayerAction.VISIBILITY_CHANGED;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new visibility changed event.
	 *
	 * @param aSource The according source (origin).
	 */
	public PlayerVisibilityChangedEvent( P aSource ) {
		super( ACTION, aSource );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return super.toString() + ", visibility := " + getSource().isVisible() + " (state before: " + ( !getSource().isVisible() ) + ")";
	}
}
