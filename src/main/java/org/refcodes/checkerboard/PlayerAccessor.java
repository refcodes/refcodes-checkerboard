// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

/**
 * Provides an accessor for a player property.
 *
 * @param <P> the generic type
 */
public interface PlayerAccessor<P extends Player<P, ?>> {

	/**
	 * Retrieves the player from the player property.
	 * 
	 * @return The player stored by the player property.
	 */
	P getPlayer();

	/**
	 * Provides a mutator for a player property.
	 *
	 * @param <P> the generic type
	 */
	public interface PlayerMutator<P extends Player<P, ?>> {

		/**
		 * Sets the player for the player property.
		 * 
		 * @param aPlayer The player to be stored by the player property.
		 */
		void setPlayer( P aPlayer );
	}

	/**
	 * Provides a builder method for a player property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <P> the generic type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PlayerBuilder<P extends Player<P, ?>, B extends PlayerBuilder<P, B>> {

		/**
		 * Sets the player for the player property.
		 * 
		 * @param aPlayer The player to be stored by the player property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPlayer( P aPlayer );
	}

	/**
	 * Provides a player property.
	 *
	 * @param <P> the generic type
	 */
	public interface PlayerProperty<P extends Player<P, ?>> extends PlayerAccessor<P>, PlayerMutator<P> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Player} (setter)
		 * as of {@link #setPlayer(Player)} and returns the very same value
		 * (getter).
		 * 
		 * @param aPlayer The {@link Player} to set (via
		 *        {@link #setPlayer(Player)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default P letPlayer( P aPlayer ) {
			setPlayer( aPlayer );
			return aPlayer;
		}
	}
}
