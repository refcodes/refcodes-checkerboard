package org.refcodes.checkerboard;

import org.refcodes.graphical.ViewportOffset;
import org.refcodes.graphical.ViewportOffsetImpl;

/**
 * The Class ViewportOffsetChangedEvent.
 *
 * @param <P> the generic type
 * @param <S> the generic type
 */
public class ViewportOffsetChangedEvent<P extends Player<P, S>, S> extends AbstractCheckerboardViewerEvent<P, S> implements ViewportOffset {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final CheckerboardViewerAction ACTION = CheckerboardViewerAction.VIEWPORT_OFFSET_CHANGED;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final int _offsetX;
	private final int _offsetY;
	private final ViewportOffset _precedingOffset;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new viewport offset changed event.
	 *
	 * @param aOffsetY the offset Y
	 * @param aOffsetX the offset X
	 * @param aPrecedingOffsetX the preceding offset X
	 * @param aPrecedingOffsetY the preceding offset Y
	 * @param aSource The according source (origin).
	 */
	public ViewportOffsetChangedEvent( int aOffsetY, int aOffsetX, int aPrecedingOffsetX, int aPrecedingOffsetY, CheckerboardViewer<P, S, ?> aSource ) {
		super( ACTION, aSource );
		_offsetX = aOffsetX;
		_offsetY = aOffsetY;
		_precedingOffset = new ViewportOffsetImpl( aPrecedingOffsetX, aPrecedingOffsetY );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportOffsetX() {
		return _offsetX;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportOffsetY() {
		return _offsetY;
	}

	/**
	 * Gets the preceding viewport offset.
	 *
	 * @return the preceding viewport offset
	 */
	public ViewportOffset getPrecedingViewportOffset() {
		return _precedingOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + _offsetX + ":" + _offsetY + ")@" + hashCode();
	}
}
