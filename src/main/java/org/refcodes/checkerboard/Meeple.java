// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

import org.refcodes.graphical.Position;

/**
 * As the {@link GraphicalCheckerboardViewer} handles {@link Player} instances
 * in terms of "sprites", the most basic directly usable {@link Player} type is
 * the {@link Meeple}.
 * 
 * @param <S> the generic type of the {@link Meeple} state.
 */
public class Meeple<S> extends AbstractPlayer<Meeple<S>, S> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link Meeple}.
	 *
	 * @param aPosX the pos X
	 * @param aPosY the pos Y
	 */
	public Meeple( int aPosX, int aPosY ) {
		super( aPosX, aPosY );
	}

	/**
	 * Instantiates a new {@link Meeple}.
	 *
	 * @param aPosition the position
	 */
	public Meeple( Position aPosition ) {
		super( aPosition );
	}
}
