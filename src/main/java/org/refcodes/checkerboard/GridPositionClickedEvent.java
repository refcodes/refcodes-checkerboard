package org.refcodes.checkerboard;

import org.refcodes.graphical.Position;

/**
 * The {@link GridPositionClickedEvent} event.
 *
 * @param <P> the generic type
 */
public class GridPositionClickedEvent<P extends Player<P, S>, S> extends AbstractCheckerboardEvent<P, S> implements Position {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final CheckerboardAction ACTION = CheckerboardAction.GRID_POSITION_CLICKED;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final int _posX;
	private final int _posY;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new position changed event.
	 *
	 * @param aPosX the pos X
	 * @param aPosY the pos Y
	 * @param aSource The according source (origin).
	 */
	public GridPositionClickedEvent( int aPosX, int aPosY, Checkerboard<P, S> aSource ) {
		super( ACTION, aSource );
		_posX = aPosX;
		_posY = aPosY;
	}

	/**
	 * Instantiates a new position changed event.
	 *
	 * @param aPosition the position
	 * @param aSource The according source (origin).
	 */
	public GridPositionClickedEvent( Position aPosition, Checkerboard<P, S> aSource ) {
		super( ACTION, aSource );
		_posX = aPosition.getPositionX();
		_posY = aPosition.getPositionY();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPositionX() {
		return _posX;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPositionY() {
		return _posY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return super.toString() + ", x := " + _posX + ", y := " + _posY;
	}
}
