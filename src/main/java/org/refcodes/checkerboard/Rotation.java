// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

/**
 * Rotation in terms of "clockwise" or "anti-clockwise". Most useful for getting
 * the next state from a {@link VonNeumannNeighbourhood} or
 * {@link MooreNeighbourhood} element by calling its
 * {@link Neighbourhood#next(Rotation)} method.
 */
public enum Rotation {
	/**
	 * Represents a clockwise rotation.
	 */
	CLOCKWISE,

	/**
	 * Represents an anti-clockwise rotation.
	 */
	ANTI_CLOCKWISE;

	/**
	 * Represents a crosswise rotation.
	 */
	// CROSSWISE,

	/**
	 * Represents an anti-crosswise rotation.
	 */
	// ANTI_CROSSWISE;

	/**
	 * Inverts the rotation direction. A {@link #CLOCKWISE} rotation becomes an
	 * {@link #ANTI_CLOCKWISE} rotation and vice versa.
	 * 
	 * @return The inverted ("the other way round") rotation.
	 */
	public Rotation inverse() {
		return switch ( this ) {
		case ANTI_CLOCKWISE -> CLOCKWISE;
		case CLOCKWISE -> ANTI_CLOCKWISE;
		};
	}

	/**
	 * Returns the next neighbourhood relative to the provided neighbourhood.
	 * Depending on the {@link Rotation} state, either
	 * {@link Neighbourhood#clockwiseNext()} {@link #CLOCKWISE} or
	 * {@link Neighbourhood#clockwisePrevious()} {@link #ANTI_CLOCKWISE}
	 * 
	 * @param <N> The neighbourhood such as the {@link VonNeumannNeighbourhood}
	 *        or the {@link MooreNeighbourhood}.
	 * @param aNeighbourhood The current state for which to retrieve the next
	 *        neighbourhood.
	 * 
	 * @return The next state relative to the given state.
	 */
	public <N extends Neighbourhood<N>> N next( N aNeighbourhood ) {
		return switch ( this ) {
		case ANTI_CLOCKWISE -> aNeighbourhood.clockwisePrevious();
		case CLOCKWISE -> aNeighbourhood.clockwiseNext();
		};
	}
}
