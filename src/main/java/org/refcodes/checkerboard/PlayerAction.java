package org.refcodes.checkerboard;

/**
 * The Enum PlayerAction.
 */
public enum PlayerAction {

	CHANGE_POSITION,

	POSITION_CHANGED,

	CLICKED,

	STATE_CHANGED,

	VISIBILITY_CHANGED,

	DRAGGABILITY_CHANGED
}
