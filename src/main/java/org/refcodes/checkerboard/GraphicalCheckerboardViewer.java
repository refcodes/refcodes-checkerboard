package org.refcodes.checkerboard;

import org.refcodes.checkerboard.BackgroundFactoryAccessor.BackgroundFactoryBuilder;
import org.refcodes.checkerboard.BackgroundFactoryAccessor.BackgroundFactoryProperty;
import org.refcodes.checkerboard.SpriteFactoryAccessor.SpriteFactoryBuilder;
import org.refcodes.checkerboard.SpriteFactoryAccessor.SpriteFactoryProperty;
import org.refcodes.graphical.ContainerDimension;
import org.refcodes.graphical.ContainerMetrics;
import org.refcodes.graphical.DragOpacityAccessor.DragOpacityBuilder;
import org.refcodes.graphical.DragOpacityAccessor.DragOpacityProperty;
import org.refcodes.graphical.FieldDimension.FieldDimensionBuilder;
import org.refcodes.graphical.FieldDimension.FieldDimensionProperty;
import org.refcodes.graphical.MoveModeAccessor.MoveModeBuilder;
import org.refcodes.graphical.MoveModeAccessor.MoveModeProperty;
import org.refcodes.graphical.Raster;
import org.refcodes.graphical.ScaleModeAccessor.ScaleModeBuilder;
import org.refcodes.graphical.ScaleModeAccessor.ScaleModeProperty;
import org.refcodes.graphical.VisibleAccessor.VisibleBuilder;
import org.refcodes.graphical.VisibleAccessor.VisibleProperty;

/**
 * The Interface GraphicalCheckerboardViewer.
 *
 * @param <P> The type representing a {@link Player}
 * @param <S> The type which's instances represent a {@link Player} state.
 * @param <IMG> the generic type
 * @param <SF> the generic type
 * @param <BF> the generic type
 * @param <CBV> The {@link CheckerboardViewer}'s type implementing this
 *        interface.
 */
public interface GraphicalCheckerboardViewer<P extends Player<P, S>, S, IMG, SF extends SpriteFactory<IMG, S, ?>, BF extends BackgroundFactory<IMG>, CBV extends GraphicalCheckerboardViewer<P, S, IMG, SF, BF, CBV>> extends CheckerboardViewer<P, S, CBV>, SpriteFactoryProperty<SF>, SpriteFactoryBuilder<SF, CBV>, BackgroundFactoryProperty<BF>, BackgroundFactoryBuilder<BF, CBV>, ContainerDimension, ContainerMetrics, FieldDimensionProperty, FieldDimensionBuilder<CBV>, Raster, VisibleProperty, VisibleBuilder<CBV>, DragOpacityProperty, DragOpacityBuilder<CBV>, MoveModeProperty, MoveModeBuilder<CBV>, ScaleModeProperty, ScaleModeBuilder<CBV> {}
