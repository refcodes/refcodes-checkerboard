// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.checkerboard;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.graphical.GridMode;

/**
 * The Class CheckerboardTest.
 *
 * @author steiner
 */
public class CheckerboardTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testEvenCheckerboardPositions() {
		final Checkerboard<Meeple<String>, String> theCheckerboard = new AbstractCheckerboard<Checkerboard<Meeple<String>, String>, Meeple<String>, String>() {}.withGridDimension( 3, 3 );
		final Meeple<String> thePlayer = new Meeple<>( 1, 1 );
		final Meeple<String> theTopPlayer = new Meeple<>( 1, 0 );
		final Meeple<String> theLeftPlayer = new Meeple<>( 0, 1 );
		final Meeple<String> theBottomPlayer = new Meeple<>( 1, 2 );
		final Meeple<String> theRightPlayer = new Meeple<>( 2, 1 );
		theCheckerboard.putPlayer( theTopPlayer );
		theCheckerboard.putPlayer( theLeftPlayer );
		theCheckerboard.putPlayer( theBottomPlayer );
		theCheckerboard.putPlayer( theRightPlayer );
		assertTrue( theCheckerboard.hasAtTopOf( thePlayer ) );
		assertTrue( theCheckerboard.hasAtLeftOf( thePlayer ) );
		assertTrue( theCheckerboard.hasAtBottomOf( thePlayer ) );
		assertTrue( theCheckerboard.hasAtRightOf( thePlayer ) );
		assertFalse( theCheckerboard.hasAtTopLeftOf( thePlayer ) );
		assertFalse( theCheckerboard.hasAtBottomLeftOf( thePlayer ) );
		assertFalse( theCheckerboard.hasAtBottomRightOf( thePlayer ) );
		assertFalse( theCheckerboard.hasAtTopRightOf( thePlayer ) );
		assertEquals( theTopPlayer, theCheckerboard.atTopOf( thePlayer ) );
		assertEquals( theRightPlayer, theCheckerboard.atRightOf( thePlayer ) );
		assertEquals( theBottomPlayer, theCheckerboard.atBottomOf( thePlayer ) );
		assertEquals( theLeftPlayer, theCheckerboard.atLeftOf( thePlayer ) );
		assertNull( theCheckerboard.atTopRightOf( thePlayer ) );
		assertNull( theCheckerboard.atBottomRightOf( thePlayer ) );
		assertNull( theCheckerboard.atBottomLeftOf( thePlayer ) );
		assertNull( theCheckerboard.atTopLeftOf( thePlayer ) );
	}

	@Test
	public void testOddCheckerboardPositions() {
		final Checkerboard<Meeple<String>, String> theCheckerboard = new AbstractCheckerboard<Checkerboard<Meeple<String>, String>, Meeple<String>, String>() {}.withGridDimension( 3, 3 );
		final Meeple<String> thePlayer = new Meeple<>( 1, 1 );
		final Meeple<String> theTopRightPlayer = new Meeple<>( 2, 0 );
		final Meeple<String> theBottomRightPlayer = new Meeple<>( 2, 2 );
		final Meeple<String> theBottomLeftPlayer = new Meeple<>( 0, 2 );
		final Meeple<String> theTopLeftPlayer = new Meeple<>( 0, 0 );
		theCheckerboard.putPlayer( theTopRightPlayer );
		theCheckerboard.putPlayer( theBottomRightPlayer );
		theCheckerboard.putPlayer( theBottomLeftPlayer );
		theCheckerboard.putPlayer( theTopLeftPlayer );
		assertFalse( theCheckerboard.hasAtTopOf( thePlayer ) );
		assertFalse( theCheckerboard.hasAtLeftOf( thePlayer ) );
		assertFalse( theCheckerboard.hasAtBottomOf( thePlayer ) );
		assertFalse( theCheckerboard.hasAtRightOf( thePlayer ) );
		assertTrue( theCheckerboard.hasAtTopLeftOf( thePlayer ) );
		assertTrue( theCheckerboard.hasAtBottomLeftOf( thePlayer ) );
		assertTrue( theCheckerboard.hasAtBottomRightOf( thePlayer ) );
		assertTrue( theCheckerboard.hasAtTopRightOf( thePlayer ) );
		assertEquals( theTopRightPlayer, theCheckerboard.atTopRightOf( thePlayer ) );
		assertEquals( theBottomRightPlayer, theCheckerboard.atBottomRightOf( thePlayer ) );
		assertEquals( theBottomLeftPlayer, theCheckerboard.atBottomLeftOf( thePlayer ) );
		assertEquals( theTopLeftPlayer, theCheckerboard.atTopLeftOf( thePlayer ) );
		assertNull( theCheckerboard.atTopOf( thePlayer ) );
		assertNull( theCheckerboard.atBottomOf( thePlayer ) );
		assertNull( theCheckerboard.atLeftOf( thePlayer ) );
		assertNull( theCheckerboard.atRightOf( thePlayer ) );
	}

	@Test
	public void testPeriodicCheckerboardPositions() {
		final Checkerboard<Meeple<String>, String> theCheckerboard = new AbstractCheckerboard<Checkerboard<Meeple<String>, String>, Meeple<String>, String>() {}.withGridDimension( 3, 3 ).withGridMode( GridMode.PERIODIC );
		final Meeple<String> theTopPlayer = new Meeple<>( 1, 0 );
		final Meeple<String> theLeftPlayer = new Meeple<>( 0, 1 );
		final Meeple<String> theBottomPlayer = new Meeple<>( 1, 2 );
		final Meeple<String> theRightPlayer = new Meeple<>( 2, 1 );
		theCheckerboard.putPlayer( theTopPlayer );
		theCheckerboard.putPlayer( theLeftPlayer );
		theCheckerboard.putPlayer( theBottomPlayer );
		theCheckerboard.putPlayer( theRightPlayer );
		assertTrue( theCheckerboard.hasAtBottomOf( theBottomPlayer ) );
		assertTrue( theCheckerboard.hasAtRightOf( theRightPlayer ) );
		assertTrue( theCheckerboard.hasAtBottomOf( theBottomPlayer ) );
		assertTrue( theCheckerboard.hasAtLeftOf( theLeftPlayer ) );
	}

	@Test
	public void testClosedCheckerboardPositions() {
		final Checkerboard<Meeple<String>, String> theCheckerboard = new AbstractCheckerboard<Checkerboard<Meeple<String>, String>, Meeple<String>, String>() {}.withGridDimension( 3, 3 ).withGridMode( GridMode.CLOSED );
		final Meeple<String> theTopPlayer = new Meeple<>( 1, 0 );
		final Meeple<String> theLeftPlayer = new Meeple<>( 0, 1 );
		final Meeple<String> theBottomPlayer = new Meeple<>( 1, 2 );
		final Meeple<String> theRightPlayer = new Meeple<>( 2, 1 );
		theCheckerboard.putPlayer( theTopPlayer );
		theCheckerboard.putPlayer( theLeftPlayer );
		theCheckerboard.putPlayer( theBottomPlayer );
		theCheckerboard.putPlayer( theRightPlayer );
		try {
			theCheckerboard.hasAtBottomOf( theBottomPlayer );
			fail( "Expected an index out of bounds exception." );
		}
		catch ( IndexOutOfBoundsException e ) {}
		try {
			theCheckerboard.hasAtRightOf( theRightPlayer );
			fail( "Expected an index out of bounds exception." );
		}
		catch ( IndexOutOfBoundsException e ) {}
		try {
			theCheckerboard.hasAtBottomOf( theBottomPlayer );
			fail( "Expected an index out of bounds exception." );
		}
		catch ( IndexOutOfBoundsException e ) {}
		try {
			theCheckerboard.hasAtLeftOf( theLeftPlayer );
			fail( "Expected an index out of bounds exception." );
		}
		catch ( IndexOutOfBoundsException e ) {}
	}
}
